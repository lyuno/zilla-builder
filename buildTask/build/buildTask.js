var fs = require('fs');
var url = require('url');
var http = require('http');
var path = require('path');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var file = require('./file');
var verify = require('./verify');

var db = require('../models/db');
var Task = require('../models/buildTask.js');

var iosBuild = require('./iosBuildTask');
var request = require('request');

//打包任务数组
global.taskArray = new Array();
//打包排队数组
global.queueArray = new Array();
//打包任务限制
global.taskLimit = 5;

 
exports.index = function(req, res) {
	if (req.param('id') != 'ios') {
		
		res.writeHead(404, {
			"Content-Type": "text/plain;charset:utf-8"
		});
		res.write("校对失败" + info);
		res.end();
		return;
	};
	console.log("req.body =="+req.body.msg);
	var args = req.body; //arg1 => {name:'a',id:'5'} 
	verify.verify(args, function(issuccess, info) {
		if (issuccess) {
			var msgInfo = args.msg;
			var msgJson = JSON.parse(msgInfo);
			if (taskArray.length < taskLimit) {
				var newTask = new Task({
					task_id: msgJson.task_id,
					identifier: msgJson.identifier,
					version: msgJson.version,
					build: msgJson.build,
					appkey: msgJson.appkey,
					certificate: msgJson.certificate,
					secretKey: msgJson.password,
					provision: msgJson.provision,
					statue: '1',
					createTime: (new Date()).toLocaleString(),
					finishTime: '',
					buildInfo: '',
					platform: req.param('id'),
					buildLog: '',
					msg: msgInfo,
					ipaFilePath: '',
					shameName: msgJson.shameName
				});
				Task.get(newTask.task_id, function(err, task) {
					if (!task) {
						newTask.save(function(err, task) {
							if (err) {
								console.log("服务器连接失败" + err);
								res.writeHead(500, {
									"Content-Type": "text/plain;charset:utf-8"
								});
								res.write("数据库失败" + err);
								res.end();
								return;
							} else {
								res.writeHead(200, {
									"Content-Type": "text/plain",
									"Content-Encoding": "utf-8"
								});
								res.write("校对成功 ", "utf8");
								res.end();
								// taskArray.push(task);
								
								//封装参数
								var downImgs=new Array();
								
								//icon  57-min 114-mid 120-max   (iphone)  72-Default 76-min  144-mid 152-max   (ipad)
								if (msgJson.icons) {
									var iconPhones = msgJson.icons.phones;
									var iconPads = msgJson.icons.pad;
									if (iconPhones) {
										if (iconPhones.min) {downImgs.push('{"key":"57","url":"'+iconPhones.min+'"}');};
										if (iconPhones.mid) {downImgs.push('{"key":"114","url":"'+iconPhones.mid+'"}');};
										if (iconPhones.max) {downImgs.push('{"key":"120","url":"'+iconPhones.max+'"}');};
									};
									if (iconPads) {
										if (iconPads.default) {downImgs.push('{"key":"72","url":"'+iconPads.default+'"}');};
										if (iconPads.min) {downImgs.push('{"key":"76","url":"'+iconPads.min+'"}');};
										if (iconPads.mid) {downImgs.push('{"key":"144","url":"'+iconPads.mid+'"}');};
										if (iconPads.max) {downImgs.push('{"key":"152","url":"'+iconPads.max+'"}');};
									};
								};
								//启动页 DefaultPage
								//phone 320 * 480 Default phone_default  |  640 * 960 Default@2x phone_min | 640 * 1136 Default~h568@2x phone_max  
								if (msgJson.defaultPages) {
									var pagePhones = msgJson.defaultPages.phones;
									if (pagePhones) {
										if (pagePhones.min) {downImgs.push('{"key":"Default","url":"'+pagePhones.min+'"}');};
										if (pagePhones.mid) {downImgs.push('{"key":"Default@2x","url":"'+pagePhones.mid+'"}');};
										if (pagePhones.max) {downImgs.push('{"key":"Default-568h@2x","url":"'+pagePhones.max+'"}');};
									};
									var pagePads = msgJson.defaultPages.pad;
									if (pagePads) {
										//pad  1024 × 748 --pad_default Default-Landscape~ipad  | 2048 × 1496 -- Default-Landscape@2x~ipad pad_max
										if (pagePads.default) {downImgs.push('{"key":"Default-Landscape2~ipad","url":"'+pagePads.default+'"}');};
										if (pagePads.default) {downImgs.push('{"key":"Default-Landscape~ipad","url":"'+pagePads.default+'"}');};
										if (pagePads.max) {downImgs.push('{"key":"Default-Landscape2@2x~ipad","url":"'+pagePads.max+'"}');};
										if (pagePads.max) {downImgs.push('{"key":"Default-Landscape@2x~ipad","url":"'+pagePads.max+'"}');};
									
									};
								};

								var downFiles = [];
								downFiles.push('{"key":"P12","url":"'+msgJson.certificate+'"}');//
								downFiles.push('{"key":"mobileprovision","url":"'+msgJson.provision+'"}');//
								console.log("downImgs ========================="+downImgs);
								iosBuild.build(null, downImgs, downFiles, newTask.task_id, newTask.identifier, newTask,msgJson, 'bsl-sdk', function(statue, task, path, buildInfo, buildLog) {
									console.log(statue + 'buildLog = ' + buildLog    );
									if (statue) {
										task.statue = '3';
									} else {
										task.statue = '4';
									};
									task.finishTime = (new Date()).toLocaleString();
									task.buildInfo = buildInfo;
									task.buildLog = buildLog;
									task.ipaFilePath = path;
									Task.update(task,function(error,bars){


										if (error) {
											console.log("服务器连接失败" + error);
										} else {
											//保存失败   -- 如何处理

										};

										if (msgJson.callback) {
											console.log("callback = "+msgJson.callback);
											request(msgJson.callback, function(error, response, body) {
												console.log("response.statusCode = "+response.statusCode );
												console.log("body = ================================================ " );
												console.log("body = "+body );
												console.log("body = ================================================" );
												if (!error && response.statusCode == 200) {
													
												}
											})
										};
									});

								});
							}
						});
					} else {
						res.writeHead(400, {
							"Content-Type": "text/plain",
							"Content-Encoding": "utf-8"
						});
						res.write("任务已经存在", "utf8");
						res.end();
						return;
					}
				});
			}

		} else {
			console.log("校对失败"+info);
			res.writeHead(400, {
				"Content-Type": "text/plain;charset:utf-8"
			});
			res.write("校对失败1" + info);
			res.end();
		};
	});
};