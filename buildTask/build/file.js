var fs = require('fs');
var url = require('url');
var http = require('http');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

var async = require("async");
var path = require("path");
var request = require("request");


function saveFile(fileUrl, data, callback) {
	console.log(fileUrl);
	fs.exists(path.dirname(fileUrl), function(exists) {
		if (!exists) {
			fs.mkdirSync(path.dirname(fileUrl));
		}
		fs.writeFile(fileUrl, data, function(err) {
			if (err) {
				console.log();
				callback(false, err);
				return;
			};
		});
		callback(true);
	});
}

function downFile(downfileUrl, savefileUrl,cb) {
	var file_url = downfileUrl;
	var DOWNLOAD_DIR = savefileUrl;
	// We will be downloading the files to a directory, so make sure it's there
	// This step is not required if you have manually created the directory
	var foldName = path.dirname(DOWNLOAD_DIR);
	console.log("foldName= " + foldName);
	var mkdir = 'mkdir -p ' + foldName;
	var child = exec(mkdir, function(err, stdout, stderr) {
		if (err) throw err;
		else download_file_httpget(file_url);
	});

	// Function to download file using HTTP.get]
	var download_file_httpget = function(file_url) {
		console.log("file_url===="+file_url);
		var file ;
		http.get(file_url, function(res) {
			res.on('data', function(data) {
				if (data && res.statusCode == 200) {
					if (!file) {
						file = fs.createWriteStream(DOWNLOAD_DIR);
					};
					file.write(data);
				}
			}).on('end', function() {
				if (file) {
					file.end();
				};
				console.log(' downloaded to ' + DOWNLOAD_DIR);
				cb();
			}).on('error', function(e) {
  				console.log('problem with request: ' + e.message);
  				
			});
		});
	};
}

// single file copy
function copyFile(file, toDir, cb) {
	async.waterfall([

		function(callback) {
			fs.exists(toDir, function(exists) {
				if (exists) {
					callback(null, false);
				} else {
					callback(null, true);
				}
			});
		},
		function(need, callback) {
			if (need) {
				mkdirs(path.dirname(toDir), callback);
			} else {
				callback(null, true);
			}
		},
		function(p, callback) {
			var reads = fs.createReadStream(file);
			var writes = fs.createWriteStream(path.join(path.dirname(toDir), path.basename(file)));
			reads.pipe(writes);
			//don't forget close the  when  all the data are read
			reads.on("end", function() {
				writes.end();
				callback(null);
			});
			reads.on("error", function(err) {
				console.log("error occur in reads");
				callback(true, err);
			});

		}
	], cb);

}

function copyDir(from, to, cb) {
	if (!cb) {
		cb = function() {};
	}
	async.waterfall([

		function(callback) {
			fs.exists(from, function(exists) {
				if (exists) {
					callback(null, true);
				} else {
					console.log(from + " not exists");
					callback(true);
				}
			});
		},
		function(exists, callback) {
			fs.stat(from, callback);
		},
		function(stats, callback) {
			if (stats.isFile()) {
				// one file copy
				copyFile(from, to, function(err) {
					if (err) {
						// break the waterfall
						callback(true);
					} else {
						callback(null, []);
					}
				});
			} else if (stats.isDirectory()) {
				ccoutTask(from, to, callback);
			}
		},
		function(files, callback) {
			// prevent reaching to max file open limit		    
			async.mapLimit(files, 10, function(f, cb) {
				copyFile(f.file, f.dir, cb);
			}, callback);
		}
	], cb);
}



function removeFold(path) {
    var files = [];
    if( fs.existsSync(path) ) {
        files = fs.readdirSync(path);
        files.forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.statSync(curPath).isDirectory()) { // recurse
                removeFold(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

exports.removeFold = removeFold;
exports.downFile = downFile;
exports.saveFile = saveFile;