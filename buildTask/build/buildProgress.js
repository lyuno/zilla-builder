var db = require('../models/db');
var Task = require('../models/buildTask.js');


exports.index = function(req, res) {
	var taskId = req.param('id');
	Task.get(taskId, function(err, task) {
		if (!task) {
			console.log("任务不存在");
			res.writeHead(404, {
				"Content-Type": "text/plain",
				"Content-Encoding": "utf-8"
			});
			res.write("任务不存在", "utf8");
			res.end();
		} else {
			var info = {};
			var taskMessage = '';
			switch (parseInt(task.statue)) {
				case 1:
					taskMessage = "任务排队中";
					break;
				case 2:
					taskMessage = "任务进行中";
					break;
				case 3:
					taskMessage = "任务成功";
					break;
				case 4:
					taskMessage = "任务失败";
					break;
			}
			console.log("taskMessage == " + taskMessage);
			info.task_status = task.statue;
			info.msg = taskMessage;
			if (task.ipaFilePath) {
				info.packageUrl = "http://10.108.1.242:8080/attachment/ipa/"+task.task_id;
			};
			if (task.buildLog) {
				info.logUrl = "http://10.108.1.242:8080/attachment/log/"+task.task_id;
			};
			var infostr = JSON.stringify(info);
			console.log("infostr"+infostr);

			res.writeHead(200, {
				"Content-Type": "text/plain",
				"Content-Encoding": "utf-8"
			});
			res.write(infostr,"utf-8");
			res.end();
		};
	});
};