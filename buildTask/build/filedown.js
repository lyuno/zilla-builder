var http = require('http');
var fs = require('fs');
var path = require('path');

var db = require('../models/db');
var Task = require('../models/buildTask.js');

function downFile(req, res) {
	//
	var id = req.param('id');
	var type = req.param('type');
	console.log('id = ' + id + '    || type = ' + type);

	Task.get(id, function(err, task) {

		if (!task) {
			console.log("1");
			write404(req, res);
			return;
		}
		var filePath='';
		console.log("task.buildLog = "+ task.buildLog );
		console.log("task.ipaFilePath ="+ task.ipaFilePath);
		if (type == 'log') {
			filePath = task.buildLog;
		} else {
			filePath = task.ipaFilePath;
		}
		if (!filePath) {
			console.log("2");
			write404(req, res);
			return;
		};
		filePathDown(filePath,req, res);
		return;
	});

}

function filePathDown(filePath, req, res) {
	fs.exists(filePath, function(exists) {
		if (!exists) {
			console.error('找不到文件' + filePath);
			write404(req, res);
			return;
		} else {
			fs.stat(filePath, function(err, stat) {
				if (stat.isFile()) {
					fs.readFile(filePath, 'binary', function(err, file) {
						if (err) {
							writeError(err, req, res);
						} else {
							
							res.writeHead(200, {
								"Content-Type": path.extname(filePath),
								"Content-Length": Buffer.byteLength(file, 'binary'),
								"Content-Disposition":"attachment;filename="+path.basename(filePath)
							});
							res.write(file,"binary");  
       						res.end();
       						return;
						};
					});
				} else {
					write404(req, res);
					return;
				}
			});
		}
	});
}

function writeError(err, req, res) {
	res.writeHead(404, {
		"Content-Type": "text/html;charset=utf-8",
		"Content-Length": Buffer.byteLength(err, 'utf8')
	});
	res.write(err);
	res.end();
}

function write404(req, res) {
	var body = "文件不存在:-(";
	res.writeHead(404, {
		"Content-Type": "text/html;charset=utf-8",
		"Content-Length": Buffer.byteLength(body, 'utf8')
	});
	res.write(body);
	res.end();
}

exports.index = downFile;