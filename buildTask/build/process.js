var exec = require('child_process').exec,
	childFile;
var fs = require('fs');

var file = require('./file');

function startExec(filePath, logPath, ipaPath, args, callback) {
	var sh = 'sh ' + filePath + " " + ipaPath + "  >  " + logPath;

	console.log('========== sh ' + sh);
	exec(sh, function(error, stdout, stderr) {
		console.log("error =============================================");
		console.log("error =" + error);
		console.log("error =============================================");
		if (error !== null) {
			callback(false, error, stdout);
			return;
		}
		callback(true, error, stdout);

	});

}
exports.startExec = startExec;