//
//  DataCenter.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import "DataCenter.h"

@implementation DataCenter

+(DataCenter*)defaultCenter{
    static DataCenter *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DataCenter alloc] init];
    });
    return sharedInstance;
}

-(id)init{
    self=[super init];
    
    if(self){
        self.username=DEFAULT_ACCOUNT_NAME;
    }
    
    return self;
}

@end
