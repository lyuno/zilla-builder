//
//  CViewController.h
//  Main
//
//  Created by Fanty on 13-12-23.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CViewController;
@protocol CViewControllerDelegate <NSObject>

-(void)parentBackClick:(CViewController*)controller;

@end

@interface CViewController : UIViewController{
    UINavigationItem* navigationItem;
    UINavigationBar* navigationBar;
}
@property(nonatomic,assign) BOOL fullScreenForIPad;
@property(nonatomic,strong) NSDictionary* params;
@property(nonatomic,weak) id<CViewControllerDelegate> delegate;


//初始化 状态栏
-(void)initNavigation;

//后退事件
-(void)backClick;

//创建状态栏右键按钮

-(void)createRightNavigationItem:(NSString*)title target:(id)target action:(SEL)action;

@end
