//
//  MainUtils.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "MainUtils.h"
#import <UIKit/UIDevice.h>
#import <UIKit/UIScreen.h>
@implementation MainUtils

+(BOOL)isPad{
    return ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad);
}


+(BOOL)isIphone5{
    if([self isPad])return NO;
    return ( [UIScreen mainScreen].bounds.size.height>=500.0f || [UIScreen mainScreen].bounds.size.height>500.0f);
}

+(BOOL)isIOS7{
    return ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0f);
}

@end
