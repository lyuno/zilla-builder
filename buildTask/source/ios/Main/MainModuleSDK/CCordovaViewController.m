//
//  CCordova.m
//  Main
//
//  Created by Fanty on 13-12-20.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "CCordovaViewController.h"
#import "MainUtils.h"
@interface CCordovaViewController ()

@end

@implementation CCordovaViewController

-(id)init{
    self=[super init];
    
    if(self){
        /*
         if([[[UIDevice currentDevice] systemVersion] floatValue]>=7){
         self.edgesForExtendedLayout = UIRectEdgeNone;
         self.extendedLayoutIncludesOpaqueBars = NO;
         self.modalPresentationCapturesStatusBarAppearance = NO;
         }
         */
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    if(self.fullScreenForIPad && [MainUtils isPad]){
        self.view.frame=CGRectMake(0.0f, 0.0f, 1024.0f, ([MainUtils isIOS7]?768.0f:748.0f));
    }

    
    for(UIView* view in self.webView.subviews){
        if([view isKindOfClass:[UIScrollView class]]){
            UIScrollView* scrollView=(UIScrollView*)view;
            scrollView.showsHorizontalScrollIndicator=NO;
            scrollView.showsVerticalScrollIndicator=NO;
            scrollView.bounces=NO;
            scrollView.scrollEnabled=NO;
            break;
        }
    }
        
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    self.finishBlock=nil;
}


-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)loadAndNavigation{
    if(self.finishBlock!=nil){
        self.finishBlock(YES);
        self.finishBlock=nil;
    }
}

-(void)loadFilePageWithFileUrl:(NSURL*)fileUrl{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    self.startPage = [fileUrl absoluteString];
}
-(void)startLoadNow{
    UIView* v=self.view;
    v=nil;

}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [super webViewDidFinishLoad:webView];
    [self performSelector:@selector(loadAndNavigation) withObject:nil afterDelay:0.1f];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [super webView:webView didFailLoadWithError:error];
    if(self.finishBlock!=nil){
        self.finishBlock(NO);
        self.finishBlock=nil;
    }
}

@end
