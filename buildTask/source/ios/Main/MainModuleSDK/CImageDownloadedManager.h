//
//  CImageDownloadedManager.h
//
//
//  Created by fanty on 13-4-22.
//  Copyright (c) 2013年 fanty. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChamleonHTTPRequest;

#define NotificationImageLoadingFinish    @"gtgz_image_finish"

@protocol CImageDownloadedManagerDelegate <NSObject>


-(ChamleonHTTPRequest*)imageDownloadRequest:(NSURL*)url;
@end

/*！
 @class GTGZImageManager
 @abstract  image download and show manager
 */
@interface CImageDownloadedManager : NSObject{
    NSMutableArray* list;
}

@property(nonatomic,assign) id<CImageDownloadedManagerDelegate> delegate;

@property(nonatomic,assign) int downloadedImageTimeout;

@property(nonatomic,strong) NSString* saveImagePath;

/*
 @return the shared instance
 */
+(CImageDownloadedManager*)sharedInstance;

/*
 @property url:          download url
 @property size          image size
 @check the image is downloaded. if yes return UIImage  if not then download
 */
-(UIImage*)appendUrl:(NSString*)url size:(CGSize)size;

/*
 @property url:          download url
 @get the UIImage from url
 */
-(UIImage*)get:(NSString*)url size:(CGSize)size;

/*
 @property url:          download url
 @property size          image size
 @remove UIImage from url
 */
-(void)remove:(NSString*)url size:(CGSize)size;

/*
 @property url:   download url
 @property size          image size
 @check image is downloaded
 */
-(BOOL)checkImageIsDownloadedByUrl:(NSString*)url size:(CGSize)size;


-(ChamleonHTTPRequest*)imageDownloadRequest:(NSURL*)url;

@end
