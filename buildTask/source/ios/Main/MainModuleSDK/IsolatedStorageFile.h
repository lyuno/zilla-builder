//
//  IsolatedStorageFile.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <Foundation/Foundation.h>

@interface IsolatedStorageFile : NSObject


//目录拷贝
+(BOOL)copyFolderAtPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)aError;

//www资源目录
+(NSString*)cordovaResourceDirectory;

//www目标目录
+(NSString*)cordovaRootDirectory;

@end
