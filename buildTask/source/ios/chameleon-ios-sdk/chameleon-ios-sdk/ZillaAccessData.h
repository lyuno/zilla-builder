//
//  ApiConstant.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import <Foundation/Foundation.h>

@interface ZillaAccessData : NSObject

+(ZillaAccessData*)activeSession;

//应用app key
@property(nonatomic,strong) NSString* appKey;

//应用app secret
@property(nonatomic,strong) NSString* appSecret;

//连接服务器
@property(nonatomic,strong) NSString* urlSchemeSuffix;


//应用token
@property(nonatomic,strong) NSString* appToken;

//应用token 过期时间
@property(nonatomic,strong) NSDate* appTokeExpired;

//是否使用sandboxEnviroment 环境
@property(nonatomic,assign) BOOL sandBoxEnviroment;


@end
