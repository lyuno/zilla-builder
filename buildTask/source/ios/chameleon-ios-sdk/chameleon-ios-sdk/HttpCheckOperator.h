//
//  BaseCheckOperator.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <Foundation/Foundation.h>

@class ChamleonHTTPRequest;

@interface HttpCheckOperator : NSObject{
    int _statusCode;
    int _errorCode;
    NSString* _errorMessage;
}

//check http response
-(BOOL)checkResponseString:(ChamleonHTTPRequest*)request;

-(int)statusCode;

-(int)errorCode;

-(NSString*)errorMessage;


@end
