//
//  RoutingModel.m
//  ChamleonSDK
//
//  Created by Fanty on 13-12-24.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "RoutingModel.h"
#import "MappingModel.h"

@implementation RoutingModel{
    NSArray* mappings;
}

-(id)init{
    self=[super init];
    if(self){
        
    }
    return self;
}

-(void)parserWithArray:(NSArray*)array{
    NSMutableArray* list=[[NSMutableArray alloc] initWithCapacity:1];
    for(NSDictionary* dict in array){
        MappingModel* mappingModel=[[MappingModel alloc] init];
        NSString* key=[[dict allKeys] objectAtIndex:0];
        [mappingModel parserWithKey:key value:[dict objectForKey:key]];
        [list addObject:mappingModel];
    }
    mappings=list;
}

-(int)maxCompareArray:(NSArray*)array{
    int currentCompare=0;
    for(MappingModel* _mappingModel in mappings){
        int maxCompare=[_mappingModel compareLinks:array];
        if(maxCompare>currentCompare){
            currentCompare=maxCompare;
        }
    }
    return currentCompare;
}

-(NSString*)mappingIdentifier:(NSArray*)array outputParams:(NSMutableDictionary*)outputParams{
    int currentCompare=0;
    MappingModel* mappingModel=nil;
    for(MappingModel* _mappingModel in mappings){
        int maxCompare=[_mappingModel compareLinks:array];
        if(maxCompare>currentCompare){
            currentCompare=maxCompare;
            mappingModel=_mappingModel;
        }
    }

    if(mappingModel!=nil){
        [outputParams setValuesForKeysWithDictionary:[mappingModel params:array]];
    }
    return mappingModel.pageIdentifier;
    
}

@end
