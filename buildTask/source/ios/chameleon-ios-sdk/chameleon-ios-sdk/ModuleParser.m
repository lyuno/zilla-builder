//
//  CubeModuleParser.m
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-27.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "ModuleParser.h"
#import "CubeModel.h"

@implementation ModuleParser

-(void)onParser:(id)jsonMap{
    
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
        
        NSMutableArray* list=[jsonMap objectForKey:@"modules"];
        for(NSDictionary* dict in list){
            CubeModel* model=[[CubeModel alloc] init];
            model.icon=[dict objectForKey:@"icon"];
            model.isAutoShow=[[dict objectForKey:@"isAutoShow"] boolValue];
            model.autoDownload=[[dict objectForKey:@"autoDownload"] boolValue];
            model.hasPrivileges=YES;//([dict objectForKey:@"privileges"]!=nil);
            model.build=[[dict objectForKey:@"build"] intValue];
            model.version=[dict objectForKey:@"version"];
            model.category=[dict objectForKey:@"category"];
            model.hidden=[[dict objectForKey:@"hidden"] boolValue];
            model.name=[dict objectForKey:@"name"];
            model.timeUnit=[dict objectForKey:@"timeUnit"];
            model.bundle=[dict objectForKey:@"bundle"];
            model.releaseNote=[dict objectForKey:@"releaseNote"];
            model.local=[dict objectForKey:@"local"];
            model.identifier=[dict objectForKey:@"identifier"];
            model.showIntervalTime=[dict objectForKey:@"showIntervalTime"];
            model.sortingWeight=[[dict objectForKey:@"sortingWeight"] intValue];
            
            id status=[dict objectForKey:@"status"];
            if(status!=nil){
                model.status=(CubeMoudleStatus)[status intValue];
            }
            
            id unReadCount=[dict objectForKey:@"unReadCount"];
            if(unReadCount!=nil){
                model.unReadCount=[unReadCount intValue];
            }
            
            id moduleBadge=[dict objectForKey:@"moduleBadge"];
            if(moduleBadge!=nil){
                model.moduleBadge=[moduleBadge boolValue];
            }
            
            [array addObject:model];
            
            model=nil;
        }
        
        result=array;
        
    }
}


@end

