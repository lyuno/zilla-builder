//
//  DBStorageUpdater.h
//  IsolatedStorageDB
//
//  Created by Fanty on 13-12-25.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBStorageUpdater : NSObject

+(void)createNewTable:(NSString*)originDB  newDB:(NSString*)newDB;

@end
