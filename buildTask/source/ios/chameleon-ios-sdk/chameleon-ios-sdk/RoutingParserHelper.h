//
//  RoutingParserHelper.h
//  ChamleonSDK
//
//  Created by Fanty on 13-12-24.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoutingParserHelper : NSObject

+(RoutingParserHelper*)sharedRouting;

//读取配置信息，传入模块标识，模块名称
-(void)readConfig:(NSString*)configJSON moduleIdentifier:(NSString*)moduleIdentifier moduleName:(NSString*)moduleName;


//传入url 返回原生模块
-(NSString*)moduleIdentifierForLink:(NSString*)urlLink identifier:(NSString*)identifier;


//传入url 返回原生模块的页面标识
-(NSString*)pageIdentifierForLink:(NSString*)urlLink identifier:(NSString*)identifier outParams:(NSMutableDictionary*)param;

/*



//返回模块开头的html5 标识
-(NSString*)html5ModuleIdentifierForLink:(NSString*)urlLink;
*/

@end
