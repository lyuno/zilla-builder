//
//  AppInfoModel.h
//  Main
//
//  Created by Fanty on 13-12-18.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

//应用版本模型
@interface AppInfoModel : NSObject

@property(nonatomic,strong) NSString* platform;

@property(nonatomic,strong) NSString* modifiedAt;

@property(nonatomic,assign) int build;

@property(nonatomic,strong) NSString* bundle;

@property(nonatomic,strong) NSString* identifier;

@property(nonatomic,strong) NSString* version;

@property(nonatomic,strong) NSString* plist;

@property(nonatomic,strong) NSString* releaseNote;

@property(nonatomic,strong) NSURL* downloadURL;

@end
