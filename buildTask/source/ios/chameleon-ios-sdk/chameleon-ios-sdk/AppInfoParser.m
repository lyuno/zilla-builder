//
//  AppInfoParser.m
//  Main
//
//  Created by Fanty on 13-12-18.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "AppInfoParser.h"
#import "AppInfoModel.h"

@implementation AppInfoParser

-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        NSDictionary* dict=(NSDictionary*)jsonMap;
        AppInfoModel* model=[[AppInfoModel alloc] init];
        model.platform=[dict objectForKey:@"platform"];
        model.modifiedAt=[dict objectForKey:@"modifiedAt"];
        model.build=[[dict objectForKey:@"build"] intValue];
        model.bundle=[dict objectForKey:@"bundle"];
        model.identifier=[dict objectForKey:@"identifier"];
        model.version=[dict objectForKey:@"version"];
        model.plist=[dict objectForKey:@"plist"];
        model.releaseNote=[dict objectForKey:@"releaseNote"];
        result=model;
    }
}


@end
