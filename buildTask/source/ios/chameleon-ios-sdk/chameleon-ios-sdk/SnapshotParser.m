//
//  SnapshotParser.m
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-27.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "SnapshotParser.h"

@implementation SnapshotParser

-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSArray class]]){
        NSArray* obj =jsonMap;
        
        NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:1];
        
        for(NSDictionary* val in obj){
            [array addObject:[val objectForKey:@"snapshot"]];
        }
        result=array;
    }
    
}


@end
