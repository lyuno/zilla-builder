//
//  AsyncRequestWorkGroup.m
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-27.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "AsyncRequestWorkGroup.h"

#import "ChamleonHTTPRequest.h"
#import "ThreadTask.h"
#import "JSONParser.h"
#import "HttpCheckOperator.h"

@interface AsyncRequestWorkGroup()

@property(nonatomic,strong) ThreadTask* threadTask;

-(void)finish;
@end

@implementation AsyncRequestWorkGroup

-(id)init{
    self=[super init];
    if(self){
        self.checkOperator=[[HttpCheckOperator alloc] init];
    }
    return self;
}

-(void)dealloc{
    self.threadTask=nil;
    self.checkOperator=nil;
    self.request=nil;
    self.parser=nil;
}

-(void)cancel{
    self.finishBlock=nil;
    [self.threadTask cancel];
    [self.request setCompletionBlock:nil];
    [self.request setFailedBlock:nil];
    [self.request cancel];
}

-(void)finish{
#ifdef DEBUG
    NSLog(@"success xml:%@",[self.request responseString]);
#endif
    
    self.threadTask=[ThreadTask asyncStart:^{
        BOOL ret=YES;
        if(self.checkOperator!=nil)
            ret=[self.checkOperator checkResponseString:self.request];
        if(ret && self.parser!=nil)
            [self.parser parse:[self.request responseData]];
    } end:^{
        if(self.finishBlock!=nil){
            self.finishBlock();
        }
        [self cancel];
    }];
}

-(void)start{
    __block AsyncRequestWorkGroup* selfObj=self;
    [self.request setCompletionBlock:^{
        [selfObj finish];
    }];
    
    [self.request setFailedBlock:^{
        [selfObj finish];
    }];
    
    [self.request start];
    
}

-(id)result{
    return [self.parser getResult];
}

-(int)statusCode{
    return [self.checkOperator statusCode];
}

-(int)errorCode{
    return [self.checkOperator errorCode];
}

-(NSString*)errorMessage{
    return [self.checkOperator errorMessage];
    
}

-(NSData*)responseData{
    return [self.request responseData];
}

@end



