//
//  CModule.h
//  mocha
//
//  Created by Justin Yip on 8/22/13.
//
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class CModule;

@protocol CApplicationDelegate <NSObject>
-(void)pushViewController:(UIViewController*)viewController animated:(BOOL)animated;

-(void)presentViewController:(UIViewController*)viewController animated:(BOOL)animated;
@end

//
// BSL Application
//
//
@interface CApplication : NSObject<UIApplicationDelegate>{
    id<CApplicationDelegate> uiApplication;
}

+(CApplication *)sharedApplication;

//obtain a module
-(CModule*)moduleForIdentifier:(NSString*)identifier;


//返回模块列表
-(NSArray*)modules;

//配置模块
-(void)initModulesConfig;

//开启第一个启动模块
-(void)startModule;

//线程执行模块操作
-(void)dispatchAsyncModules;



- (BOOL)application:(id<CApplicationDelegate>)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

//返回模块工程第一次uiviewcontroller
-(UIViewController*)startMainViewControllerWithIdentifier:(NSString*)identifier params:(NSDictionary*)params;

//跳转模块，推入的方式
-(void)redirectToModulePage:(NSString*)identifier params:(NSDictionary*)params;

//跳转模块，弹出式
-(void)presentToModulePage:(NSString*)identifier params:(NSDictionary*)params;

//模块重定位
-(UIViewController*)moduleForViewControllerWithIdentifier:(NSString*)identifier pageIdentifier:(NSString*)pageIdentifier;

//跳转页面，推入的方式
-(void)redirectToPage:(UIViewController*)viewController;

//跳转页面，弹出式
-(void)presentToPage:(UIViewController*)viewController;

@end