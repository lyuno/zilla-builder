//
//  MessageModel.m
//  Message
//
//  Created by Fanty on 13-12-17.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "MessageModel.h"

@implementation MessageModel

-(id)init{
    self=[super init];
    
    self.messageId=@"";
    self.title=@"";
    self.content=@"";
    self.moduleIdentifer=@"";
    self.moduleName=@"";
    self.announceId=@"";
    self.messageType=@"";
    
    return self;
}

@end

