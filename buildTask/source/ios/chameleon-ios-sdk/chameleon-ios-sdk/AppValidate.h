//
//  AppValidate.h
//  Main
//
//  Created by Fanty on 13-12-18.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

//应用验证模型
@interface AppValidate : NSObject

@property(nonatomic,strong) NSString* token;
@property(nonatomic,strong) NSDate* expired;

@end
