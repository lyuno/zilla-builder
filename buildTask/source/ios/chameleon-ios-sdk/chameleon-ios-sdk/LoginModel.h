//
//  LoginModel.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import <Foundation/Foundation.h>

//登陆帐号模型
@interface LoginModel : NSObject

@property(nonatomic,assign) BOOL hasOperation;
@property(nonatomic,strong) NSString* accountToken;

@end
