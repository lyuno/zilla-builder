//
//  Zilla.h
//  Zilla
//
//  Created by Fanty on 13-12-26.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZillaDelegate.h"

@class Zilla;
@class ZillaAccessData;
@class ZillaUrlScheme;
@class AsyncRequestWorkGroup;


@interface Zilla : NSObject

//应用数据封装
@property(nonatomic,readonly) ZillaAccessData* zillAccessData;

//应用接口封装
@property(nonatomic,readonly) ZillaUrlScheme* zillaUrlScheme;

//回调代理
@property(nonatomic,weak) id<ZillaDelegate> delegate;

//初始化
-(id)initWithZillAccessData:(ZillaAccessData*)zillAccessData;

//初始化
-(id)initWithAppKey:(NSString*)appKey appSecret:(NSString*)appSecret sandBoxEnviroment:(BOOL)sandBoxEnviroment;

//初始化
-(id)initWithAppKey:(NSString*)appKey appSecret:(NSString*)appSecret sandBoxEnviroment:(BOOL)sandBoxEnviroment urlSchemeSuffix:(NSString*)urlSchemeSuffix;

-(void)cancelAllWorkGroup;

-(void)cancelCheckAppValidate;

-(void)cancelCheckAppVersion;

-(void)cancelLogin;

-(void)cancelSyncModules;

-(void)cancelSnapshot;

-(void)cancelSyncReceipts;

-(void)cancelFeedBack;

-(void)cancelRolePriviligesListTask;

//应用验证
-(void)checkAppVaildate;

//应用版本验证
-(void)checkAppVersion;

//应用签到，
//deviceToken:  应用推送的devicetoken
//accountName:  应用登陆的帐号，可以为空
//roleName：     应用的角色名，如：a,b,c
//sandBoxEnviroment:  是否使用sandbox 环境
-(void)registerDevice:(NSString*)deviceToken accountName:(NSString*)accountName roleName:(NSString*)roleName;

//帐户登陆
-(void)login:(NSString*)name password:(NSString*)password;


//获取模块列表
-(void)syncModules;

//获取快照
-(void)snapshot:(NSString*)identifier version:(NSString*)version;

//获取未发回执的消息列表
-(void)syncReceipts;

//获取未发回执的消息列表
-(void)syncReceiptsWithMessageId:(NSString*)messageId;

//发送回执
-(void)feedBackReceipt:(NSArray*)array;

//获取角色权限列表
-(void)rolePriviligesList:(NSString*)accountName;





//应用版本验证，返回工作组控制类
-(AsyncRequestWorkGroup*)checkAppVersionWithWorkGroup;

//帐户登陆，返回工作组控制类
-(AsyncRequestWorkGroup*)loginWithWorkGroup:(NSString*)name password:(NSString*)password;

//获取模块列表，返回工作组控制类
-(AsyncRequestWorkGroup*)syncModulesWithWorkGroup;

//获取快照，返回工作组控制类
-(AsyncRequestWorkGroup*)snapshotWithWorkGroup:(NSString*)identifier version:(NSString*)version;

//获取未发回执的消息列表，返回工作组控制类
-(AsyncRequestWorkGroup*)syncReceiptsWithWorkGroup;

//获取未发回执的消息列表，返回工作组控制类
-(AsyncRequestWorkGroup*)syncReceiptsWithMessageIdWithWorkGroup:(NSString*)messageId;


//获取角色权限列表，返回工作组控制类
-(AsyncRequestWorkGroup*)rolePriviligesListWithWorkGroup:(NSString*)roles;


@end
