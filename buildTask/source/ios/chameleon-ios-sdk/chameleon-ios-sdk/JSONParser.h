//
//  JsonParser.h
//
//
//  Created by fanty on 13-4-28.
//  Copyright (c) 2013年 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONParser: NSObject{
    id result;
}

-(void)parse:(NSData*)data;
-(id)getResult;


@end
