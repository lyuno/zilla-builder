//
//  NavigationViewController.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-19.
//
//

#import "NavigationViewController.h"

@interface NavigationViewController ()

@end

@implementation NavigationViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && [UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPhone){
        
        UIView* bgView=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 22.0f)];
        bgView.backgroundColor=[UIColor colorWithRed:70/255.0 green:135/255.0 blue:199/255.0 alpha:1];
        [self.view addSubview:bgView];
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad)
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    else
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

-(BOOL)shouldAutorotate{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad)
        return UIInterfaceOrientationMaskLandscape;
    else
        return UIInterfaceOrientationMaskPortrait;
}


@end
