//
//  MainModule.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-5.
//
//

#import <ChamleonSDK/CModule.h>
#import "ParserDelegate.h"

@class AsyncTask;
@class ASIHTTPRequest;
@class ASIFormDataRequest;


@interface MainModule : CModule


-(AsyncTask*)asyncWithOperation:(ASIHTTPRequest*)operation parser:(id<ParserDelegate>)parser;


-(ASIFormDataRequest*)requestWithURL:(NSURL*)url;


@end
