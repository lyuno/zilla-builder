//
//  LocalDBPlugin.m
//  cube-ios
//
//  Created by chen shaomou on 4/11/13.
//
//

#import "LocalDBPlugin.h"


@implementation LocalDBPlugin

-(void)executeQuery:(CDVInvokedUrlCommand*)command{
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


@end
