//
//  BaseCheckOperator.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <Foundation/Foundation.h>

#import "CheckOperatorDelegate.h"

@interface BaseCheckOperator : NSObject<CheckOperatorDelegate>{
    int _statusCode;
    int _errorCode;
    NSString* _errorMessage;
}

@end
