//
//  MainModule.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-5.
//
//

#import "MainModule.h"
#import <ChamleonSDK/ChamleonSDK.h>
#import <ChamleonSDK/Library/UIDevice+IdentifierAddition.h>

#import "MainUtils.h"
#import "IsolatedStorageFile.h"

#import "DataCenter.h"

#import "AsyncTask.h"
#import "ASIFormDataRequest.h"
#import "ApiConstant.h"


@interface MainModule()

-(void)loginSuccess:(NSNotification*)notification;
@end

@implementation MainModule

-(id)init{
    self=[super init];
    
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess:) name:@"CubeLogin_Success" object:nil];

        
    }
    
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)application:(CApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions{
    
    
    [IsolatedStorageFile copyFolderAtPath:[IsolatedStorageFile cordovaResourceDirectory] toPath:[IsolatedStorageFile cordovaRootDirectory] error:nil];
    
    return YES;
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)_deviceToken{
    
    NSLog(@"application token did success");
    if ([_deviceToken length]<1)
        return;
    
    NSString *token = [[_deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    [DataCenter defaultCenter].deviceToken = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
}


-(void)loginSuccess:(NSNotification*)notification{
    [[CApplication sharedApplication] redirectToModulePage:@"bsl.cube" params:notification.object];

}

-(AsyncTask*)asyncWithOperation:(ASIHTTPRequest*)operation parser:(id<ParserDelegate>)parser{
    AsyncTask* task=[[AsyncTask alloc] init];
    task.request=operation;
    task.parser=parser;
    [task start];
    return task;
}


-(ASIFormDataRequest*)requestWithURL:(NSURL*)url{
    
    ASIFormDataRequest* request=[ASIFormDataRequest requestWithURL:url];
    [request setPostValue:[ApiConstant defaultConstant].appKey forKey:@"appKey"];
    [request setPostValue:[[UIDevice currentDevice] uniqueDeviceIdentifier] forKey:@"deviceId"];
    [request setPostValue:[[NSBundle mainBundle] bundleIdentifier] forKey:@"appId"];
    
    
    return request;
    
}



@end
