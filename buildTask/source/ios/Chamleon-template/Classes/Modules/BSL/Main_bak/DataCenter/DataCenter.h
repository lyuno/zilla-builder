//
//  DataCenter.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import <Foundation/Foundation.h>

@interface DataCenter : NSObject
@property(nonatomic,strong) NSString* deviceToken;

@property(nonatomic,strong) NSMutableDictionary* dictonary;

+(DataCenter*)defaultCenter;

-(void)setObject:(id)value forKey:(NSString*)key;
@end
