//
//  MainUtils.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <Foundation/Foundation.h>

@interface MainUtils : NSObject

+(BOOL)isPad;

+(BOOL)isIphone5;

@end
