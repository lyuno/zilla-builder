//
//  ApiConstant.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import <Foundation/Foundation.h>

@interface ApiConstant : NSObject

+(ApiConstant*)defaultConstant;

@property(nonatomic,strong) NSString* appKey;

@property(nonatomic,strong) NSString* serverURLHost;
//@property(nonatomic,strong) NSString* pushServerURL;
//@property(nonatomic,strong) NSString* receptHostURL;


@end
