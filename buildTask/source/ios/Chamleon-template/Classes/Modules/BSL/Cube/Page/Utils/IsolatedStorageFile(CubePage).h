//
//  CubeDataPathUtils.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import <Foundation/Foundation.h>
#import "IsolatedStorageFile.h"
@class CubeModel;

@interface IsolatedStorageFile(CubePage)

+(void)initCorvaConfig;

+(NSString*)cubeResourceDirectory;

+(NSString*)cordovaMainDirectory;


+(NSString*)dependencieModuleFile:(NSString*)identifier;

+(NSString*)cubeModuleIdentifierMainPage:(NSString*)identifier;

@end
