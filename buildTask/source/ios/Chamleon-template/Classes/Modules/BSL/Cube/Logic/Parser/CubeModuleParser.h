//
//  CubeModuleParser.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "JsonParser.h"

@class CubeModel;
@interface CubeModuleParser : JsonParser

+(NSString*)serialize:(CubeModel*)cubeModel showOutputIcon:(BOOL)showOutputIcon;
+(NSString*)parserJSONFromArray:(NSArray*)array showOutputIcon:(BOOL)showOutputIcon;
+(NSString*)handleModuleToJson:(NSDictionary*)list;
@end
