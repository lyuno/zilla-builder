//
//  CubeModuleDownloaded.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import <Foundation/Foundation.h>

#import "ICubeModule.h"
@class ASIHTTPRequest;
@class CubeModel;
@interface CubeModuleDownloaded : NSObject{
    ASIHTTPRequest* request;
    NSTimer* processTimer;
}

@property(nonatomic,strong)CubeModel* cubeModel;
@property(nonatomic,weak) id<ICubeModuleWorkDownloadedAndUnZip> callback;


-(void)cancel;

-(void)start;

@end
