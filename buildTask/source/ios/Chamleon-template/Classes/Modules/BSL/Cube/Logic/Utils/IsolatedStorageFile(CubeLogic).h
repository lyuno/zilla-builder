//
//  CubeDataPathUtils.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import <Foundation/Foundation.h>
#import "IsolatedStorageFile.h"

@class CubeModel;

@interface IsolatedStorageFile(CubeLogic)

+(NSString*)localMoudlelistDirectory:(NSString*)account;

+(NSString*)localModelInfoFile;

+(NSString*)cubeModelTempZip:(NSString*)identifier;

+(NSString*)cubeModuleIdentifierRoot:(NSString*)identifier;

+(NSString*)landIconLocalFile:(NSString*)identifier;

+(NSString*)outputIcon:(CubeModel*)cubeModel;

+(NSString*)loadAutoSaveFile:(NSString*)account;

@end
