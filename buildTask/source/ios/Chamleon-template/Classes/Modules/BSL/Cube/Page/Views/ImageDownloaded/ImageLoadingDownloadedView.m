//
//  ImageLoadingDownloadedView.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "ImageLoadingDownloadedView.h"

@implementation ImageLoadingDownloadedView
-(id)init{
    self=[super init];
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    return self;
    
}

-(void)imageDownloadedStatusChanged{
    
    if(self.status!=ImageViewDownloadedStatusFinish && self.status!=ImageViewDownloadedStatusNone){
        if(loadingView==nil){
            loadingView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            loadingView.color=[UIColor blackColor];
            [self addSubview:loadingView];
        }
        [loadingView startAnimating];
    }
    else{
        [loadingView stopAnimating];
        [loadingView removeFromSuperview];
        loadingView=nil;
    }
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGRect rect=loadingView.frame;
    rect.size=CGSizeMake(64.0f, 64.0f);
    rect.origin=CGPointMake((self.frame.size.width-rect.size.width)*0.5f, (self.frame.size.height-rect.size.height)*0.5f);
    loadingView.frame=rect;
}

@end
