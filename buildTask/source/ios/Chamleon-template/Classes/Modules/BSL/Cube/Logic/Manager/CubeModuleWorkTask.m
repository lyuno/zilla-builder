//
//  CubeModuleWorkTask.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import "CubeModuleWorkTask.h"
#import "CubeModuleDownloaded.h"
#import "CubeModuleUnZip.h"
#import "IsolatedStorageFile(CubeLogic).h"
#import "CubesModel.h"

@implementation CubeModuleWorkTask


- (void)dealloc{
    NSLog(@"CubeModuleWorkTask dealloc");
}

-(void) process{
    [self.callback process:self];
}

-(void) failed{
    NSString* filePath=[IsolatedStorageFile cubeModelTempZip:self.cubeModel.identifier];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];

    self.running=NO;
    [self.callback failed:self];

}

-(void) unzip{

    [cubeDownloaded cancel];
    cubeDownloaded=nil;
    [cubeUnziped cancel];
    
    cubeUnziped=[[CubeModuleUnZip alloc] init];
    cubeUnziped.callback=self;
    cubeUnziped.cubeModel=self.cubeModel;
    [cubeUnziped start];
}

-(void)success{
    NSString* filePath=[IsolatedStorageFile cubeModelTempZip:self.cubeModel.identifier];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    self.running=NO;
    [self.callback success:self];
}

-(void)cancel{
    self.running=NO;
    
    [cubeDownloaded cancel];
    [cubeUnziped cancel];
    
    cubeDownloaded=nil;
    cubeUnziped=nil;
}


-(void)start{
    [self cancel];
    self.running=YES;
    [self performSelector:@selector(process) withObject:nil afterDelay:0.1f];
    cubeDownloaded=[[CubeModuleDownloaded alloc] init];
    cubeDownloaded.cubeModel=self.cubeModel;
    cubeDownloaded.callback=self;
    [cubeDownloaded start ];

}



@end
