//
//  CubeModuleAutoSaveParser.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import "JsonParser.h"

@interface CubeModuleAutoSaveParser : JsonParser

+(NSString*)parserJSONFromArray:(NSArray*)array;

@end
