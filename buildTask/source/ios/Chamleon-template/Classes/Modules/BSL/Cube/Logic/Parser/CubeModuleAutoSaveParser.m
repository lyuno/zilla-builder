//
//  CubeModuleAutoSaveParser.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import "CubeModuleAutoSaveParser.h"

@implementation CubeModuleAutoSaveParser

-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        
        NSMutableArray* list=[[NSMutableArray alloc] initWithCapacity:2];
        NSDictionary* array=jsonMap;
        [array enumerateKeysAndObjectsUsingBlock:^(id key,id obj,BOOL*stop){
            [list addObject:obj];
        }];
        result=list;
    }
}

+(NSString*)parserJSONFromArray:(NSArray*)array{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{"];
    [array enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL* stop){
        @autoreleasepool {
            NSString* identifier=obj;
            [str appendFormat:@"\"%d\":\"%@\"",index,identifier];
            if(index<[array count]-1)
                [str appendString:@","];
        }
    }];
    [str appendString:@"}"];
    return str;
}

@end
