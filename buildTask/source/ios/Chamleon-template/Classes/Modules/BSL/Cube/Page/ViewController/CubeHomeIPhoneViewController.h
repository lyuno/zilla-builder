//
//  MainIPhoneViewController.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import <ChamleonSDK/CCordovaViewController.h>


@class AutoDownloadCheckingOperator;
@class KKProgressToolbar;
@interface CubeHomeIPhoneViewController : CCordovaViewController{
    AutoDownloadCheckingOperator* autoDownloadCheckingOperator;
    
    KKProgressToolbar* statusToolbar;
}




@end
