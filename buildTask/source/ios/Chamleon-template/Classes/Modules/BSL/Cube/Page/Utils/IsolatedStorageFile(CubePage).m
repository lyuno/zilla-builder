//
//  CubeDataPathUtils.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import "IsolatedStorageFile(CubePage).h"
#import <ChamleonSDK/CApplication.h>
#import "MainUtils.h"
#import "IsolatedStorageFile.h"

@implementation IsolatedStorageFile(CubePage)

+(void)initCorvaConfig{

}

+(NSString*)cubeResourceDirectory{
    NSURL* url= [[[NSBundle mainBundle] bundleURL] URLByAppendingPathComponent:@"main_www" isDirectory:YES];
    
    return [url path];

}

+(NSString*)cordovaMainDirectory{

    NSString* path=[[IsolatedStorageFile cordovaRootDirectory] stringByAppendingPathComponent:([MainUtils isPad]?@"pad/index.html":@"phone/index.html")];
    return path;
    
}

+(NSString*)dependencieModuleFile:(NSString*)identifier{
    
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];
    
    root=[root stringByAppendingPathComponent:identifier];
    
    return [root stringByAppendingPathComponent:@"CubeModule.json"];
}

+(NSString*)cubeModuleIdentifierMainPage:(NSString*)identifier{
    
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];
    
    root=[root stringByAppendingPathComponent:identifier];
    
    return [root stringByAppendingPathComponent:@"index.html"];

}

@end
