//
//  SnapshotParser.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "SnapshotParser.h"

@implementation SnapshotParser

-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSArray class]]){
        NSArray* obj =jsonMap;
        
        NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:1];
        
        for(NSDictionary* val in obj){
            [array addObject:[val objectForKey:@"snapshot"]];
        }
        result=array;
    }
    
}


@end
