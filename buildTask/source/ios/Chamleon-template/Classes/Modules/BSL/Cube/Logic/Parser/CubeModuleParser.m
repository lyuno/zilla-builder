//
//  CubeModuleParser.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "CubeModuleParser.h"
#import "CubesModel.h"
#import "IsolatedStorageFile(CubeLogic).h"
@implementation CubeModuleParser


-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
        
        NSMutableArray* list=[jsonMap objectForKey:@"modules"];
        for(NSDictionary* dict in list){
            CubeModel* model=[[CubeModel alloc] init];
            model.icon=[dict objectForKey:@"icon"];
            model.isAutoShow=[[dict objectForKey:@"isAutoShow"] boolValue];
            model.autoDownload=[[dict objectForKey:@"autoDownload"] boolValue];
            model.hasPrivileges=([dict objectForKey:@"privileges"]!=nil);
            model.build=[[dict objectForKey:@"build"] intValue];
            model.version=[dict objectForKey:@"version"];
            model.category=[dict objectForKey:@"category"];
            model.hidden=[[dict objectForKey:@"hidden"] boolValue];
            model.name=[dict objectForKey:@"name"];
            model.timeUnit=[dict objectForKey:@"timeUnit"];
            model.bundle=[dict objectForKey:@"bundle"];
            model.releaseNote=[dict objectForKey:@"releaseNote"];
            model.local=[dict objectForKey:@"local"];
            model.identifier=[dict objectForKey:@"identifier"];
            model.showIntervalTime=[dict objectForKey:@"showIntervalTime"];
            model.sortingWeight=[[dict objectForKey:@"sortingWeight"] intValue];

            id status=[dict objectForKey:@"status"];
            if(status!=nil){
                model.status=(CubeMoudleStatus)[status intValue];
            }
            
            id messageCount=[dict objectForKey:@"messageCount"];
            if(messageCount!=nil){
                model.messageCount=[messageCount intValue];
            }

            id moduleBadge=[dict objectForKey:@"moduleBadge"];
            if(moduleBadge!=nil){
                model.moduleBadge=[moduleBadge boolValue];
            }
            
            [array addObject:model];
            
            model=nil;
        }
        
        result=array;
        
    }
}

+(NSString*)serialize:(CubeModel*)model  showOutputIcon:(BOOL)showOutputIcon{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{"];
    
    [str appendFormat:@"\"category\":\"%@\",",[model.category stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];

    if(showOutputIcon)
        [str appendFormat:@"\"icon\":\"%@\",",[IsolatedStorageFile outputIcon:model]];
    else
        [str appendFormat:@"\"icon\":\"%@\",",[model.icon stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    if(!showOutputIcon){
        [str appendFormat:@"\"isAutoShow\":%@,",(model.isAutoShow?@"true":@"false") ];
        [str appendFormat:@"\"autoDownload\":%@,",(model.autoDownload?@"true":@"false") ];
        if(model.hasPrivileges)
            [str appendString:@"\"privileges\":\"\"," ];
    }
    [str appendFormat:@"\"build\":%d,",model.build ];
    [str appendFormat:@"\"version\":\"%@\",",[model.version stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    [str appendFormat:@"\"hidden\":%@,",(model.hidden?@"true":@"false") ];
    [str appendFormat:@"\"name\":\"%@\",",[model.name stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    if(!showOutputIcon){
        [str appendFormat:@"\"timeUnit\":\"%@\",",[model.timeUnit stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
        [str appendFormat:@"\"bundle\":\"%@\",",[model.bundle stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    }
    [str appendFormat:@"\"releaseNote\":\"%@\",",[model.releaseNote stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    if([model.local length]<1)
        [str appendString:@"\"local\":\"\","];
    else
        [str appendFormat:@"\"local\":\"%@\",",[model.local stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    [str appendFormat:@"\"identifier\":\"%@\",",[model.identifier stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    if(!showOutputIcon){
        [str appendFormat:@"\"showIntervalTime\":\"%@\",",[model.showIntervalTime stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    }
    [str appendFormat:@"\"sortingWeight\":%d,",model.sortingWeight ];
    
    if(!showOutputIcon){
        [str appendFormat:@"\"status\":%d,",model.status ];
    
        [str appendFormat:@"\"messageCount\":%d,",model.messageCount ];
        
        [str appendFormat:@"\"moduleBadge\":%@",(model.moduleBadge?@"true":@"false") ];

    }
    else{
        [str appendFormat:@"\"msgCount\":%d",model.messageCount ];
    }
    
    
    [str appendString:@"}"];

    return str;
}

+(NSString*)parserJSONFromArray:(NSArray*)array showOutputIcon:(BOOL)showOutputIcon{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{\"result\":\"success\",\"modules\":["];
    [array enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL* stop){
        @autoreleasepool {
            CubeModel* model=(CubeModel*)obj;
            [str appendString:[CubeModuleParser serialize:model showOutputIcon:showOutputIcon]];
            if(index<[array count]-1)
                [str appendString:@","];
        }
    }];
    [str appendString:@"]}"];
    
    return str;
}

+(NSString*)handleModuleToJson:(NSDictionary*)list{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{"];
    
    [list.allKeys enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL*stop){
        
        @autoreleasepool {
            NSString* cubeModuleKey=obj;

            NSArray* array=[list objectForKey:cubeModuleKey];
            [str appendFormat:@"\"%@\"",cubeModuleKey];
            [str appendString:@":["];
            

            [array enumerateObjectsUsingBlock:^(id _obj,NSUInteger _index,BOOL*_stop){
                CubeModel* model=(CubeModel*)_obj;
                [str appendString:[CubeModuleParser serialize:model showOutputIcon:YES]];
                if(_index<[array count]-1)
                    [str appendString:@","];
            }];
            
            [str appendString:@"]"];
            
            if (index< [list.allKeys count]-1){
                //不是最后一个元素，得加上逗号
                [str appendString:@","];
            }
            
        }
    }];
    
    [str appendString:@"}"];
    return str;
}

@end
