//
//  LoginManager.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import <Foundation/Foundation.h>

#import "ICubeModule.h"

@class AsyncTask;
@class CubeModel;

@interface CubeModuleManager : NSObject<ICubeModuleWorkTask>{
    AsyncTask* syncTask;
    
    NSMutableArray* asyncs;
    BOOL asyncIsDownloaded;
}

-(void)syncModuleList:(void (^)(BOOL success))callback;

-(AsyncTask*)loadSnapshotByIdentify:(NSString*)identifier version:(NSString*)version;

-(void)loadLocalModuleList;

-(void)saveLocalModuleList;

-(CubeModel*)findDownloadingCubeModule:(NSString*)identifier;

-(void)cancel;

-(int)downloadCount;

-(void)installModule:(NSString*)identifier;

-(void)uninstallModule:(NSString*)identifier;

-(void)updateModule:(NSString*)identifier;

@end
