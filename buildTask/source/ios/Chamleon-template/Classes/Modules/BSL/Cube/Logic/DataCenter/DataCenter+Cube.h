//
//  DataCenter+Cube.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "DataCenter.h"

@class CubeModel;

@interface DataCenter(Cube)


@property(nonatomic,strong) NSMutableArray* localModules;
@property(nonatomic,strong) NSMutableArray* installMoudules;
@property(nonatomic,strong) NSArray* serviceModules;


-(NSArray*)mainListModules;

-(NSArray*)uninstallModules;

-(NSArray*)updatableModules;

-(NSDictionary*)mainListCategroyMap;

-(NSDictionary*)uninstallCategroyMap;

-(NSDictionary*)installCategroyMap;

-(NSDictionary*)updatableCategroyMap;

-(CubeModel*)finidInstallCubeModule:(NSString*)identifier;

-(CubeModel*)finidServiceCubeModule:(NSString*)identifier;


@end
