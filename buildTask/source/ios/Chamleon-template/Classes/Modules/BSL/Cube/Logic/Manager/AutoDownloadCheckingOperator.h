//
//  AutoDownloadCheckingOperator.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import <Foundation/Foundation.h>


@class AutoDownloadCheckingOperator;

@protocol AutoDownloadCheckingOperatorDelegate <NSObject>

-(void)autoSaveDidShowAutoDownloading:(AutoDownloadCheckingOperator*)cubeModuleAutoSaveParser message:(NSString*)message;

-(void)autoSaveDidShowUpdating:(AutoDownloadCheckingOperator*)cubeModuleAutoSaveParser message:(NSString*)message;

@end


@interface AutoDownloadCheckingOperator : NSObject{
    NSMutableArray* downloadModels;
}


@property(nonatomic,weak) id<AutoDownloadCheckingOperatorDelegate> delegate;
@property(nonatomic,assign) int autoDownloadedCount;
@property(nonatomic,readonly) NSArray* downloadModels;

-(void)clear;
-(void)start;


-(NSArray*)dependsList:(NSString*)identifier;

@end
