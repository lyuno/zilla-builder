//
//  MainModule.m
//  bsl-sdk
//
//  Created by Fanty on 13-11-28.
//
//

#import "CubeModule.h"
#import "CubeHomeIPhoneViewController.h"
#import "IsolatedStorageFile(CubePage).h"


@implementation CubeModule
@synthesize cubeModuleManager;
@synthesize autoDownloadCheckingOperator;

- (BOOL)application:(CApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions{
    cubeModuleManager=[[CubeModuleManager alloc] init];
    autoDownloadCheckingOperator=[[AutoDownloadCheckingOperator alloc] init];

    [IsolatedStorageFile copyFolderAtPath:[IsolatedStorageFile cubeResourceDirectory] toPath:[IsolatedStorageFile cordovaRootDirectory] error:nil];
    
    return YES;
}

-(UIViewController*)startMainViewControllerWithParams:(NSDictionary*)dict{
    if(dict!=nil){
        self.username=[dict objectForKey:@"name"];
        self.sessionKey=[dict objectForKey:@"sessionKey"];
    }
    return [[CubeHomeIPhoneViewController alloc] init];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
}

@end
