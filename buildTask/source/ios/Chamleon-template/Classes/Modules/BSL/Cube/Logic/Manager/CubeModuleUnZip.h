//
//  CubeModuleUnZip.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import <Foundation/Foundation.h>
#import "ICubeModule.h"

@class CubeModel;

@interface CubeModuleUnZip : NSObject{
    NSThread* thread;
    BOOL running;
}

@property(nonatomic,strong)CubeModel* cubeModel;
@property(nonatomic,weak) id<ICubeModuleWorkDownloadedAndUnZip> callback;


-(void)cancel;

-(void)start;

@end
