//
//  LandScroller.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <UIKit/UIKit.h>

@interface LandScroller : UIImageView{
    UIScrollView* scrollView;
    NSMutableArray* images;
}


-(void)setImages:(NSArray*)array;

@end
