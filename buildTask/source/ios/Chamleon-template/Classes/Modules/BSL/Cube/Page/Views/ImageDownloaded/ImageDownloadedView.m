//
//  ImageDownloadedView.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "ImageDownloadedView.h"

@implementation ImageDownloadedView

-(id)init{
    self=[super init];
    
    if(self){
        defaultView=[[UIImageView alloc] init];
        defaultView.image=[UIImage imageNamed:@"default notload.png"];
        [self addSubview:defaultView];

    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    
    if(self){
        defaultView=[[UIImageView alloc] initWithFrame:self.bounds];
        defaultView.image=[UIImage imageNamed:@"default notload.png"];
        [self addSubview:defaultView];
        
    }
    return self;

}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if(self){
        defaultView=[[UIImageView alloc] init];
        defaultView.image=[UIImage imageNamed:@"default notload.png"];
        [self addSubview:defaultView];
        
    }
    return self;
}


-(void)imageDownloadedStatusChanged{
    if(self.status!=ImageViewDownloadedStatusFinish && self.status!=ImageViewDownloadedStatusNone){
        if(defaultView==nil){
            defaultView=[[UIImageView alloc] initWithFrame:self.bounds];
            defaultView.image=[UIImage imageNamed:@"default notload.png"];
            [self addSubview:defaultView];
        }
    }
    else{
        [defaultView removeFromSuperview];
        defaultView=nil;
    }
}

-(void)layoutSubviews{
    defaultView.frame=self.bounds;
    [super layoutSubviews];

}

-(void)removeLoading{
    [defaultView removeFromSuperview];
    defaultView=nil;

}

@end
