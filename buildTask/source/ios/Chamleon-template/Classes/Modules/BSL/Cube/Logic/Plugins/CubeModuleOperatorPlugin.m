//
//  CubeModuleOperator.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "CubeModuleOperatorPlugin.h"
#import <ChamleonSDK/CApplication.h>
#import "CubeModule.h"
#import "CubeModuleManager.h"
#import "BSLCubeConstant.h"


@implementation CubeModuleOperatorPlugin

-(void)sync:(CDVInvokedUrlCommand*)command{
    
    NSString* callbackId = command.callbackId;
    
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"bsl.cube"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CubeSyncStart object:nil];
    
    [module.cubeModuleManager syncModuleList:^(BOOL success){
        CDVPluginResult* pluginResult=nil;
        if (success){
            NSLog(@"同步成功");
            [[NSNotificationCenter defaultCenter] postNotificationName:CubeSyncSuccess object:nil];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"同步成功"];
        }
        else{
            NSLog(@"同步失败");
            [[NSNotificationCenter defaultCenter] postNotificationName:CubeSyncFailed object:nil];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"同步失败"];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        
    }];
}

-(void)setting:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;
    [[NSNotificationCenter defaultCenter] postNotificationName:CubeSettingPage object:nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)manager:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;
    [[NSNotificationCenter defaultCenter] postNotificationName:ModuleMainRedirectManager object:nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)index:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;
    [[NSNotificationCenter defaultCenter] postNotificationName:ModuleMainRedirectMain object:nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)showModule:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;

    NSString* identifier =  [[command.arguments objectAtIndex:0] lowercaseString];
    
    NSString* type =  [command.arguments objectAtIndex:1];

    if ([type isEqualToString:@"main"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleWeb object:identifier];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleRunningLanding object:command.arguments];
    }
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}


-(void)install:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;
    
    NSString* identifier =  [[command.arguments objectAtIndex:0] lowercaseString];
    
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"bsl.cube"];
    [module.cubeModuleManager installModule:identifier];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

-(void)upgrade:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;
    
    NSString* identifier =  [[command.arguments objectAtIndex:0] lowercaseString];
    
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"bsl.cube"];
    [module.cubeModuleManager updateModule:identifier];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}


-(void)uninstall:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;
    
    NSString* identifier =  [[command.arguments objectAtIndex:0] lowercaseString];
    
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"bsl.cube"];
    [module.cubeModuleManager uninstallModule:identifier];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)setTheme:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)fragmenthide:(CDVInvokedUrlCommand*)command{
    NSString* callbackId = command.callbackId;
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}


@end
