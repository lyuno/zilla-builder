//
//  LoginPathUtils.h
//  bsl-sdk
//
//  Created by Fanty on 13-11-28.
//
//

#import <Foundation/Foundation.h>
#import <MainModuleSDK/IsolatedStorageFile.h>

@interface IsolatedStorageFile(Login)

//登陆www源目录
+(NSString*)loginResourceDirectory;

//登陆www目标目录
+(NSString*)cordovaLoginDirectory;

@end
