//
//  LoginViewController.m
//  bsl-sdk
//
//  Created by Fanty on 13-11-28.
//
//

#import "LoginViewController.h"

#import "IsolatedStorageFile+Login.h"
#import "SVProgressHUD.h"
#import "LoginModule.h"
#import <chameleon-ios-sdk/ChamleonSDK.h>
#import <MainModuleSDK/DataCenter.h>
#import <MainModuleSDK/MainUtils.h>

@interface LoginViewController ()
-(void)backClick;
-(void)loginStart;
-(void)loginSuccess;
-(void)loginFailed:(NSNotification*)notification;
@end

@implementation LoginViewController

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
        self.fullScreenForIPad=YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginStart) name:@"CubeLogin_Start" object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess) name:@"CubeLogin_Success" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginFailed:) name:@"CubeLogin_Failed" object:nil];

        
        
    }
    return self;
}

- (void)viewDidLoad{
    NSURL* url=[NSURL fileURLWithPath:[IsolatedStorageFile cordovaLoginDirectory]];
    [self loadFilePageWithFileUrl:url];
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0){
        
        UIView* bgView=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 30.0f)];
        bgView.backgroundColor=[UIColor colorWithRed:70/255.0 green:135/255.0 blue:199/255.0 alpha:1];
        [self.view addSubview:bgView];
    }
    
    float topOffset=0.0f;
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0f)
        topOffset=20.0f;
    
    UINavigationBar* navigationBar=[[UINavigationBar alloc] initWithFrame:CGRectMake(0.0f, topOffset, self.view.frame.size.width, 44.0f)];
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0){
        navigationBar.barTintColor = [UIColor colorWithRed:70/255.0 green:135/255.0 blue:199/255.0 alpha:1];
        navigationBar.translucent = NO;
        UIColor * cc = [UIColor whiteColor];
        NSDictionary * dict = [NSDictionary dictionaryWithObject:cc forKey:UITextAttributeTextColor];
        navigationBar.titleTextAttributes = dict;
    }
    else{
        [navigationBar setTintColor: [UIColor colorWithRed:70/255.0 green:135/255.0 blue:199/255.0 alpha:1]];
    }
    
    UINavigationItem* navigationItem=[[UINavigationItem alloc] initWithTitle:@"登陆"];

    UIImage* img=[UIImage imageNamed:@"Main.bundle/images/back.png"];
    UIButton* backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    float w=([MainUtils isIOS7]?img.size.width:img.size.width*2.0f);
    backButton.frame=CGRectMake(0.0f, 0.0f, w, img.size.height);
    [backButton setImage:img forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    
    
    [navigationBar pushNavigationItem:navigationItem animated:NO];
    [self.view addSubview:navigationBar];

    
    CGRect rect=self.webView.frame;
    rect.origin.y=CGRectGetMaxY(navigationBar.frame);
    rect.size.height=self.view.frame.size.height-rect.origin.y;
    self.webView.frame=rect;
    
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark method

-(void)backClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loginStart{
    [SVProgressHUD showWithStatus:@"登陆中..." maskType:SVProgressHUDMaskTypeBlack];
}

-(void)loginSuccess{
    /*
    LoginModule* loginModule=(LoginModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Login"];
    LoginManager* loginManager=loginModule.loginManager;
    AsyncTask* task=[loginManager auth];
    __block AsyncTask* __task=task;
    __block LoginViewController* objSelf=self;
    [task setFinishBlock:^{
        [SVProgressHUD dismiss];

        if([__task result]!=nil){
            [DataCenter defaultCenter].roles=[__task result];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CubeAuth_Success" object:nil];
            [loginManager syncAuth:NO];
            [objSelf dismissViewControllerAnimated:YES completion:nil];
        }
        else if([__task statusCode]==204 || [__task statusCode]==400){
            UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"应用没有为该帐户授权。" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else if([__task statusCode]==500){
            UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"服务器返回异常，请稍候再试。" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];

        }
        else{
            UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"网络异常，请检查再试。" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
     */
}

-(void)loginFailed:(NSNotification*)notification{
    [SVProgressHUD dismiss];

    UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:notification.object message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
}



@end
