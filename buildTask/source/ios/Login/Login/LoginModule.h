//
//  LoginModule.h
//  bsl-sdk
//
//  Created by Fanty on 13-11-28.
//
//

#import <chameleon-ios-sdk/CModule.h>



#import "LoginManager.h"

@interface LoginModule : CModule{
    
    LoginManager* loginManager;

}

@property(nonatomic,readonly)LoginManager* loginManager;


@end
