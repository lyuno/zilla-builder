//
//  LoginModule.m
//  bsl-sdk
//
//  Created by Fanty on 13-11-28.
//
//

#import "LoginModule.h"
#import "LoginViewController.h"

#import "IsolatedStorageFile+Login.h"
#import <chameleon-ios-sdk/CApplication.h>


@interface LoginModule()

//-(void)cube_SecurityUpdated;
@end

@implementation LoginModule

@synthesize loginManager;

-(id)init{
    self=[super init];
    
    if(self){
        
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cube_SecurityUpdated) name:@"Cube_SecurityUpdated" object:nil];
    }
    return self;
}

- (BOOL)application:(CApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions{
    
    return YES;
}

-(void)startDispatchAsync{
//    [IsolatedStorageFile copyFolderAtPath:[IsolatedStorageFile loginResourceDirectory] toPath:[IsolatedStorageFile cordovaRootDirectory] error:nil];
}

-(void)finishDispatchAsync{
    loginManager=[[LoginManager alloc] init];
    [loginManager restoreAccount];
    
}



-(UIViewController*)startMainViewControllerWithParams:(NSDictionary*)dict{
    LoginViewController* controller=[[LoginViewController alloc] init];
    return controller;
}
/*
-(void)cube_SecurityUpdated{
    [loginManager syncAuth:YES];
}
*/


@end
