//
//  AboutViewController.h
//  cube-ios
//
//  Created by 东 on 13-3-20.
//
//

#import <UIKit/UIKit.h>
#import <MainModuleSDK/CViewController.h>
@interface AboutViewController : CViewController{
    
    UIImageView* bgView;
    UIImageView* headerView;
    UILabel *appNameLabel;
    UILabel *versionLabel;
    UILabel* deviceTipLabel;
    UILabel *driviceIdLabel;
}
@end
