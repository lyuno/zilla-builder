//
//  JSUtils.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import <Foundation/Foundation.h>

@interface JSUtils : NSObject

//js 更新模块
+(NSString*)refreshModule:(NSString*)identifier type:(NSString*)type moduleMessage:(NSString*)moduleMessage;


//js 更新主页
+(NSString*)refreshMainPage:(NSString*)identifier type:(NSString*)type moduleMessage:(NSString*)moduleMessage;

//js更新消息
+(NSString*)receiveMessage:(NSString*)identifier count:(int)count;


//js 更新进程
+(NSString*)updateProgress:(NSString*)identifier count:(int)count;

//改变登陆状态
+(NSString*)loginStatus:(NSString*)status;

+(NSString*)loginOrLogout:(BOOL)login;

+(NSString*)refreshManagerPage;


@end
