//
//  ImageDownloadedView.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <MainModuleSDK/CImageDownloadedView.h>

@interface ImageDownloadedView : CImageDownloadedView{
    UIImageView* defaultView;
}


-(void)removeLoading;

@end
