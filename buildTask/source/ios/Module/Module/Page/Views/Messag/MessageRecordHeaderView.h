//
//  MessageRecordHeaderView.h
//  Cube-iOS
//
//  Created by chen shaomou on 2/1/13.
//
//

#import <UIKit/UIKit.h>

@protocol MessageRecordHeaderViewDelegate
-(void)deleteModuleData:(NSString *)moduleId;
-(void)shouldShowCellInModule:(NSString *)moduleId atIndex:(NSInteger)section;
@end

@interface MessageRecordHeaderView : UIView{
    id <MessageRecordHeaderViewDelegate> __weak delegate;
    
}
@property (nonatomic,weak)  id <MessageRecordHeaderViewDelegate> delegate;
@property (assign, nonatomic) BOOL isDeleteStatue;
@property (strong, nonatomic) UIImageView *iconImageView;
@property (strong, nonatomic) UILabel *moduleNameLabel;
@property (strong, nonatomic) UILabel *messageCountLabel;
@property (strong, nonatomic) UIImageView *mesageCountBgImageView;
@property (strong, nonatomic) UIButton* deleteBtn;
@property (strong, nonatomic) NSString*  moduleId;
@property NSInteger section;


-(void)configureWithIconUrl:(NSString *)iconurl moduleName:(NSString *)name messageCount:(NSString *)count  sectionIndenx:(NSInteger)index withUnReadCount:(NSString *)unReadCount;
@end
