//
//  AnnouncementTableViewController.m
//  Module
//
//  Created by Fanty on 13-12-22.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "AnnouncementTableViewController.h"
#import "AnnouncementTableViewCell.h"
#import "CubeModule.h"
#import <chameleon-ios-sdk/CApplication.h>
#import <chameleon-ios-sdk/MessageModel.h>

#import <MainModuleSDK/MainUtils.h>


@interface AnnouncementTableViewController ()<UITableViewDataSource,UITableViewDelegate>
-(void)createRrightNavItem;
-(void)checkSelectedAnnounceId;
-(void)rightNavClick;
-(void)reloadData;
-(void)cube_MessageUpdated;
@end

@implementation AnnouncementTableViewController

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
        self.title=@"公告";
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cube_MessageUpdated) name:@"Cube_MessageUpdated" object:nil];
        list=[[NSMutableArray alloc] initWithCapacity:2];
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self reloadData];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self initNavigation];
    
    CGRect rect=self.view.bounds;
    rect.origin.y=CGRectGetMaxY(navigationBar.frame);
    rect.size.height-=rect.origin.y;
    
    
    tableView=[[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
    tableView.backgroundColor=[UIColor whiteColor];
    tableView.delegate=self;
    tableView.dataSource=self;
    
    UIView* v=[[UIView alloc] init];
    v.backgroundColor=[UIColor clearColor];
    tableView.tableFooterView=v;
    tableView.tableHeaderView=v;
    tableView.showsHorizontalScrollIndicator=NO;
    tableView.showsVerticalScrollIndicator=NO;
    
    
    [self.view addSubview:tableView];
    
    [self createRrightNavItem];
    
    [self checkSelectedAnnounceId];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}


- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark method

-(void)rightNavClick{
    //    tableView.editing=!tableView.editing;
    [tableView setEditing:!tableView.editing animated:YES];
    [self createRrightNavItem];
    
}

-(void)createRrightNavItem{
    if([list count]<1){
        self.navigationItem.rightBarButtonItem=nil;
        return;
    }
    [self createRightNavigationItem:(tableView.editing?@"取消":@"编辑") target:self action:@selector(rightNavClick)];
    
}

-(void)checkSelectedAnnounceId{
    if([self.messageId length]>0){
        NSUInteger index=0;
        for(MessageModel* model in list){
            if([model.messageId isEqualToString:self.messageId]){
                break;
            }
            index++;
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
}

-(void)reloadData{
    [list removeAllObjects];
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    NSArray* array=[module.messageManager messageList:@"com.foss.announcement"];
    if([array count]>0)
        [list addObjectsFromArray:array];
}

-(void)cube_MessageUpdated{
    [self reloadData];
    [tableView reloadData];
    
    [self createRrightNavItem];
    
}



#pragma mark tableview delegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor=[UIColor whiteColor];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [list count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)__tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageModel* messageModel = [list objectAtIndex:[indexPath section]];
    
    return [AnnouncementTableViewCell cellHeight:messageModel.title content:messageModel.content width:__tableView.frame.size.width];
}


- (UITableViewCell *)tableView:(UITableView *)__tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AnnouncementTableViewCell *cell = [__tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[AnnouncementTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    MessageModel* messageModel = [list objectAtIndex:[indexPath section]];
    [cell title:messageModel.title content:messageModel.content time:messageModel.receiveTime isRead:messageModel.isRead];
    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)__tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(__tableView.editing)
        return UITableViewCellEditingStyleDelete;
    return UITableViewCellEditingStyleNone;
}

-(void)tableView:(UITableView *)__tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        //        获取选中删除行索引值
        NSUInteger index=[indexPath section];
        CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
        MessageModel* messageModel = [list objectAtIndex:[indexPath section]];
        
        [module.messageManager removeMesageById:messageModel.messageId];
        [list removeObjectAtIndex:index];
        
        
        [__tableView deleteSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:UITableViewRowAnimationAutomatic];
        if([list count]<1)
            self.navigationItem.rightBarButtonItem=nil;
        
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)__tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [__tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    MessageModel* messageModel = [list objectAtIndex:[indexPath section]];
    if(!messageModel.isRead){
        messageModel.isRead=YES;
        [module.messageManager setIsReadMessageById:messageModel.messageId];
        [__tableView reloadSections:[NSIndexSet indexSetWithIndex:[indexPath section]] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
    
}



@end
