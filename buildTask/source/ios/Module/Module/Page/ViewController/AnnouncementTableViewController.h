//
//  AnnouncementTableViewController.h
//  Module
//
//  Created by Fanty on 13-12-22.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <MainModuleSDK/CViewController.h>

@interface AnnouncementTableViewController : CViewController{

    UITableView* tableView;
    NSMutableArray *list;

}

@property (nonatomic,strong) NSString * messageId;


@end
