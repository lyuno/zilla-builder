//
//  TestPushViewController.m
//  Module
//
//  Created by Fanty on 13-12-25.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "TestPushViewController.h"
#import "TestPushDetailController.h"
@interface TestPushViewController ()
-(void)btnClose;
-(void)btnRedirect;
@end

@implementation TestPushViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIButton* button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame=CGRectMake(10.0f, 10.0f, 100.0f, 50.0f);
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitle:@"关闭" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnClose) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame=CGRectMake(130.0f, 10.0f, 150.0f, 50.0f);
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitle:@"跳转去推送测试详细页" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnRedirect) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    
    UILabel* label=[[UILabel alloc] initWithFrame:self.view.bounds];
    label.textAlignment=NSTextAlignmentCenter;
    label.numberOfLines=0;
    label.backgroundColor=[UIColor clearColor];
    label.font=[UIFont systemFontOfSize:20.0f];
    [self.view addSubview:label];
    
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:2];
    [str appendString:@"测试推送首页\n通过推送出的viewcontroller:\n 参数:\rn"];
    [self.params enumerateKeysAndObjectsUsingBlock:^(id key,id obj,BOOL*stop){
        [str appendFormat:@"param:%@  = value:%@\n",key,obj];
    }];
    label.text=str;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)btnClose{
    if(self.navigationController!=nil)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self dismissModalViewControllerAnimated:YES];
}

-(void)btnRedirect{
    TestPushDetailController* controller=[[TestPushDetailController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
