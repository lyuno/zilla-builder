//
//  MessageViewViewController.h
//  Message
//
//  Created by Fanty on 13-12-17.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <MainModuleSDK/CViewController.h>

@interface MessageViewController : CViewController{
    UITableView* tableView;

    NSMutableArray* groupArray;
    NSMutableArray* messageArray;
    
    NSString* selectedModuleIdentifier;
    
    BOOL refreshLoading;
}

@end
