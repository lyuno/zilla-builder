//
//  AnnouncementTableViewCell.h
//  Module
//
//  Created by Fanty on 13-12-22.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnouncementTableViewCell : UITableViewCell{
    UIView* bgView;
    UILabel* titleLabel;
    UILabel* contentLabel;
    UILabel* isReadLabel;
    UILabel* timeLabel;
    UIView* lineView;
}

+(float)cellHeight:(NSString*)title content:(NSString*)content width:(float)w ;

-(void)title:(NSString*)title content:(NSString*)content time:(NSDate*)time isRead:(BOOL)isRead;

@end
