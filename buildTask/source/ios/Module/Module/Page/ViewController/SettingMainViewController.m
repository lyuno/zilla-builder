//
//  SettingMainViewController.m
//  Module
//
//  Created by Fanty on 13-12-22.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "SettingMainViewController.h"
#import "AboutViewController.h"

#import <chameleon-ios-sdk/ChamleonSDK.h>
#import <MainModuleSDK/MainUtils.h>
#import "DataCenter+Cube.h"
#import "SVProgressHUD.h"

@interface SettingMainViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,ZillaDelegate>
@property (strong,nonatomic) NSMutableArray* settingSource;

-(void)exitBtn;
@end

@implementation SettingMainViewController

- (id)init{
    self = [super init];
    if (self) {
        self.title= @"设置";
        
        zillaSDK=[[Zilla alloc] initWithZillAccessData:[ZillaAccessData activeSession]];
        zillaSDK.delegate=self;
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    if([MainUtils isPad]){
        CGRect rect=self.view.frame;
        rect.size=CGSizeMake(540.0f, 748.0f);
        self.view.frame=rect;
    }
    
    
    self.view.backgroundColor=[UIColor clearColor];
    
    [self initNavigation];
    
    CGRect rect=self.view.bounds;
    rect.origin.y=CGRectGetMaxY(navigationBar.frame);
    rect.size.height-=rect.origin.y;
    
    
    tableView=[[UITableView alloc] initWithFrame:rect style:UITableViewStyleGrouped];
    tableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Module.bundle/images/homebackImg.png"]];
    tableView.backgroundView=nil;
    tableView.delegate=self;
    tableView.dataSource=self;
    
    [self.view addSubview:tableView];
    
    NSMutableArray* array=[[NSMutableArray alloc]init];
    self.settingSource = array;
    array=nil;
    
    NSArray* array1 = [[NSArray alloc]initWithObjects:@"  更新版本",@"Module.bundle/images/setting_update.png", nil];
    NSArray* array2 = [[NSArray alloc]initWithObjects:@"  关于我们",@"Module.bundle/images/setting_about.png", nil];
    
    [self.settingSource addObject:array1];
    [self.settingSource addObject:array2];
    
    /*
     UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0,0,UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiomPhone ? 320:640,44)];
     //添加退出登陆按钮
     
     
     UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiomPhone ? 14:28, 0, UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiomPhone ? 292: 484, 44)];
     
     [btn setBackgroundImage:[UIImage imageNamed:@"Module.bundle/images/setting_exitBg.png"] forState:UIControlStateNormal];
     [btn setTitle:@"登出当前账号" forState:UIControlStateNormal];
     [btn addTarget:self action:@selector(exitBtn) forControlEvents:UIControlEventTouchUpInside];
     
     [footerView addSubview:btn];
     
     tableView.tableFooterView = footerView;
     footerView=nil;
     */
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [zillaSDK cancelAllWorkGroup];
}


#pragma mark method

-(void)backClick{
    if([MainUtils isPad])
        [self dismissModalViewControllerAnimated:YES];
    else
        [self.navigationController popViewControllerAnimated:YES];
}


-(void)exitBtn{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"退出登录" message:@"是否确认退出登录?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 100;
    [alertView show];
}

#pragma mark tableview delegate


//========= tableview delegate start ===============
-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.settingSource count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell*)tableView:(UITableView*)__tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    UITableViewCell*cell = [__tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if(cell== nil){
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"];
    }
    cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    NSArray* array=[self.settingSource objectAtIndex:[indexPath row]];
    NSString* text=[array objectAtIndex:0];
    NSString* icon=[array objectAtIndex:1];
    cell.textLabel.text = text;
    UIImageView* imageview  = [[UIImageView alloc] initWithFrame:CGRectMake(UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiomPhone ? 25 : 50, 12.5, 20,20)];
    imageview.image = [UIImage imageNamed: icon];
    
    [cell addSubview:imageview];
    return cell;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return  @"系统设置";
}

//选中cell后的回调函数
-(void)tableView:(UITableView *)__tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [__tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0 ) {

        [zillaSDK checkAppVersion];
        [SVProgressHUD showWithStatus:@"加载中..." maskType:SVProgressHUDMaskTypeBlack];
    }else if(indexPath.row == 1){
        
        AboutViewController* aboutViewController = [[AboutViewController alloc] init];
        [self.navigationController pushViewController:aboutViewController animated:YES];
        
    }
}

#pragma mark zilla delegate
//应用版本回调
-(void)appVersionChecked:(AppInfoModel*)appInfoModel{
    [SVProgressHUD dismiss];
    int newBuild = appInfoModel.build;
    int currentBuild = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] intValue];
    if (newBuild > currentBuild){
        NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        [DataCenter defaultCenter].plist=appInfoModel.plist;
        NSString *message = [NSString stringWithFormat:@"当前版本:%@\n最新版本:%@\n版本说明:\n%@", currentVersion, appInfoModel.version, appInfoModel.releaseNote];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[ NSString stringWithFormat:@"%@平台版本更新",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"] ] message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"更新", nil];
        alertView.tag=3;
        [alertView show];
    }
    else{
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"版本更新" message:@"暂未发现有版本更新" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [av show];
        av=nil;
        
    }
}

-(void)appVersionFailed:(int)statusCode errorMessage:(NSString*)errorMessage{
    if(statusCode>0){
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"版本更新" message:@"更新出错，请检查网络" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [av show];
        av=nil;
    }
    else{
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"版本更新" message:@"暂未发现有版本更新" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [av show];
        av=nil;
        
        
    }

}

#pragma mark alertview delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if( buttonIndex==1){
        if(alertView.tag==100){// 退出登陆
            [self backClick];
        }
        else{
            NSString* downloadUrl=[zillaSDK.zillaUrlScheme attachmentByAIdWithNoAppKey:[DataCenter defaultCenter].plist];
            [[UIApplication sharedApplication] openURL:[zillaSDK.zillaUrlScheme applicationNewVersionLink:downloadUrl]];
        }
    }
}


@end
