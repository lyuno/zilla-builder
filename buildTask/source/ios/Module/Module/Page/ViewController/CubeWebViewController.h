//
//  CubeWebViewController.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-9.
//
//

#import <MainModuleSDK/CCordovaViewController.h>
#import <Cordova/CDVCommandDelegateImpl.h>

@class CubeWebViewController;
@protocol CubeWebViewControllerDelegate <NSObject>

-(void)webViewBackClick:(CubeWebViewController*)controller;

@end


@interface CubeCommandDelegate : CDVCommandDelegateImpl{
    __weak CubeWebViewController* webViewController;
}
@end



@interface CubeWebViewController : CCordovaViewController

@property(nonatomic,assign) BOOL isMessageBoxPage;
@property(nonatomic,assign) BOOL isCheckPrivileges;
@property(nonatomic,strong) NSString* url;
@property(nonatomic,strong) NSString* identifier;
@property(nonatomic,weak) id<CubeWebViewControllerDelegate> delegate;

@property(nonatomic,weak) UIViewController* rootViewController;


-(void)showHomeButton:(BOOL)showHome;
@end
