//
//  CubeLogoutPlugin.m
//  Module
//
//  Created by Fanty on 14-1-3.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import "CubeLogoutPlugin.h"

#import <chameleon-ios-sdk/ChamleonSDK.h>
#import "DataCenter+Cube.h"

#import "CubeModule.h"
#import <SVProgressHUD/SVProgressHUD.h>

@implementation CubeLogoutPlugin

-(void)logout:(CDVInvokedUrlCommand*)command{
    NSString* callbackId=command.callbackId;
    
    [DataCenter defaultCenter].accountToken=nil;
    [DataCenter defaultCenter].username=DEFAULT_ACCOUNT_NAME;
    [DataCenter defaultCenter].rolePriviligeModel=[DataCenter defaultCenter].guestRolePriviligeModel;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CubeAuth_Success" object:[NSNumber numberWithBool:YES] ];
    
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

@end
