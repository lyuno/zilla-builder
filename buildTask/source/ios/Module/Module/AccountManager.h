//
//  AccountManager.h
//  Module
//
//  Created by Fanty on 13-12-26.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountManager : NSObject

//同步用户权限信息
-(void)syncAuth:(BOOL)callNow;


@end
