//
//  DataCenter+Cube.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import <MainModuleSDK/DataCenter.h>

@class CubeModel;
@class CubeModuleJsonSerial;

@interface DataCenter(Cube)



//主页显示的模块列表
-(NSArray*)mainListModules;

//未下载模块列表
-(NSArray*)uninstallModules;

//待更新模块列表
-(NSArray*)updatableModules;

//主页显示的模块列表
//-(CubeModuleJsonSerial*)mainListCategroyMap;

-(NSDictionary*)mainListCategroyMap;


//未下载模块列表
//-(CubeModuleJsonSerial*)uninstallCategroyMap;

-(NSDictionary*)uninstallCategroyMap;


//已下载模块列表
//-(CubeModuleJsonSerial*)installCategroyMap;

-(NSDictionary*)installCategroyMap;


//待更新模块列表
//-(CubeModuleJsonSerial*)updatableCategroyMap;
-(NSDictionary*)updatableCategroyMap;

//查找已下载的模块
-(CubeModel*)finidInstallCubeModule:(NSString*)identifier;

//查找服务器模块
-(CubeModel*)finidServiceCubeModule:(NSString*)identifier;

//判断是否有权限去读取模块
-(BOOL)priviligeGetModuleByIdentifier:(NSString*)identifier;

//判断是否有权限去删除模块
-(BOOL)priviligeDeleteModuleByIdentifier:(NSString*)identifier;

@end
