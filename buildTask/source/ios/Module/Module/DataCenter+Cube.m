//
//  DataCenter+Cube.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "DataCenter+Cube.h"
#import "CubesModel.h"
#import <chameleon-ios-sdk/CubeModel.h>
#import <chameleon-ios-sdk/PriviligeModel.h>
#import <objc/runtime.h>


@implementation DataCenter(Cube)



//-(CubeModuleJsonSerial*)mainListCategroyMap{
-(NSDictionary*)mainListCategroyMap{
    NSArray* array=[self mainListModules];
    return [self moduleListToCategoryMap:array];
}


-(NSArray*)mainListModules{
    NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
    NSArray* serviceModules=self.serviceModules;
    for(CubeModel* model in serviceModules){
        //if (model.hasPrivileges){
        CubeModel* __model =[self finidInstallCubeModule:model.identifier];
        if(__model!=nil){
            if (__model.hidden == NO ||
                __model.status == CubeMoudleStatusInstalling ||
                __model.status == CubeMoudleStatusUpdating)
                [array addObject:__model];
            
        }
        //}
    }
    return array;
}

-(NSArray*)uninstallModules{
    NSMutableArray* result=[[NSMutableArray alloc] initWithCapacity:2];

    NSArray* installMoudules=self.installMoudules;
    NSArray* serviceModules=self.serviceModules;
    for (CubeModel* model in serviceModules){
        //已安装列表不包含，则为未安装模块
        //if(model.hasPrivileges){
            BOOL hasExist=NO;
            for(CubeModel* _model in installMoudules){
                if([_model.identifier isEqualToString:model.identifier]){
                    hasExist=YES;
                    break;
                }
            }
            if(!hasExist)
                [result addObject:model];
        //}
    }
    
    return result;
}

-(NSArray*)updatableModules{
    NSMutableArray* result=[[NSMutableArray alloc] initWithCapacity:2];
    
    NSArray* serviceModules=self.serviceModules;
    
    for(CubeModel* model in serviceModules){
        //if(model.hasPrivileges){
        CubeModel* __model =[self finidInstallCubeModule:model.identifier];
        if(__model!=nil){
            if(model.build>__model.build)
                [result addObject:__model];
        }
        //}
    }
    
    return result;
}


-(NSDictionary*) moduleListToCategoryMap:(NSArray*)modelList{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc] init];
    for (CubeModel* model in modelList){
        /*
         张国东 改
         对模块权限加判断
         */
        NSMutableArray* list=[dict objectForKey:model.category];
        if(list==nil){
            list=[[NSMutableArray alloc] initWithCapacity:3];
            [dict setObject:list forKey:model.category];
        }
        if([self priviligeGetModuleByIdentifier:model.identifier]){
            [list addObject:model];
        }
    }
    return dict;
}

-(NSDictionary*)uninstallCategroyMap{
    NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:1];
    
    for(CubeModel* model in self.installMoudules){
        //if(model.hasPrivileges){
        if([model.local length]<1 && (model.status==CubeMoudleStatusInstalling || model.status==CubeMoudleStatusNone))
            [array addObject:model];
        //}
    }
    
    NSArray* __array=[self uninstallModules];
    [array addObjectsFromArray:__array];
    return [self moduleListToCategoryMap:array];
}

-(NSDictionary*)installCategroyMap{
    
    NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:3];
    NSArray* serviceModules=self.serviceModules;

    for(CubeModel* model in serviceModules){
        //if(model.hasPrivileges){
        CubeModel* __model =[self finidInstallCubeModule:model.identifier];
        if([model.local length]>0 || __model.status==CubeMoudleStatusFinish){
            [array addObject:__model];
        }
        //}
    }
    return [self moduleListToCategoryMap:array];
}

-(NSDictionary*)updatableCategroyMap{
    NSArray* array=[self updatableModules];
    return [self moduleListToCategoryMap:array];

}


-(CubeModel*)finidInstallCubeModule:(NSString*)identifier{
    for (CubeModel* model in self.installMoudules){
        if([model.identifier isEqualToString:identifier]){
            return model;
        }
    }
    
    return nil;
}

-(CubeModel*)finidServiceCubeModule:(NSString*)identifier{
    for (CubeModel* model in self.serviceModules){
        if([model.identifier isEqualToString:identifier]){
            return model;
        }
    }
    
    return nil;

}

-(BOOL)priviligeGetModuleByIdentifier:(NSString*)identifier{
    NSArray* list=self.rolePriviligeModel.priviliges;
    for(PriviligeModel* model in list){
        if([model.identifier isEqualToString:identifier]){
            if(model.priviligeActionType==PriviligeActionTypeGet || model.priviligeActionType==PriviligeActionTypeDelete)
                return YES;
        }
    }
    return NO;
}

-(BOOL)priviligeDeleteModuleByIdentifier:(NSString*)identifier{
    NSArray* list=self.rolePriviligeModel.priviliges;
    for(PriviligeModel* model in list){
        if([model.identifier isEqualToString:identifier]){
            if(model.priviligeActionType==PriviligeActionTypeDelete)
                return YES;
        }
    }
    return NO;

}


@end
