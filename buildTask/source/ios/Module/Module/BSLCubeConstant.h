//
//  BSLCubeConstant.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//


#define IPAD_SIZE   CGSizeMake(508.0f, 748.0f)

/// <summary>
/// 模块下载过程事件
/// </summary>
#define CubeModuleDownloadingProcess  @"bsl-cubeMdp"

/// <summary>
/// 模块下载完成事件
/// </summary>
#define CubeModuleDownloadingSuccess @"bsl-cubeMDS"

/// <summary>
/// 模块下载失败事件
/// </summary>
#define CubeModuleDownloadingFailed  @"bsl-cubeMDF"

/// <summary>
/// 同步成功发送消息
/// </summary>
#define CubeSyncSuccess  @"bsl-CubeSyncSuccess"

/// <summary>
/// 同步失败
/// </summary>
#define CubeSyncFailed  @"bsl-CubeSyncFailed"

/// <summary>
/// 跳转至模块内部
/// </summary>
#define CubeModuleWeb  @"bsl-cubeModuleWeb"

/// <summary>
/// 通过apns跳转至模块内部
/// </summary>

#define CubeAPNSPushModuleWeb @"bsl-cubeAPNSPushModuleWeb"


/// <summary>
/// 跳转模块快照页面
/// </summary>
#define CubeModuleRunningLanding @"bsl-cubeRMLLanding"

/// <summary>
/// 跳转至设置页面
/// </summary>
#define CubeSettingPage @"bsl-cubeSettingPage"

/// <summary>
/// 模块操作完成时刷新界面
/// </summary>
#define CubeRefreshModule @"bsl-cubeRefreshModule"

/// <summary>
/// 自动下载完成刷新整个界面
/// </summary>
#define CubeRefreshMainPage @"bsl-cubeRefreshMainPage"

/// <summary>
/// 刷新推送的数字
/// </summary>
#define CubeReceiveMessage @"bsl-cubeReceiveMessage"

/// <summary>
/// 刷新进度条
/// </summary>
#define CubeUpdateProgress @"bsl-cubeUpdateProgress"

/// <summary>
/// 模块同步准备
/// </summary>
#define CubeSyncStart @"bsl-cubeSyncStart"

/// <summary>
/// 由主页跳入管理页
/// </summary>
#define ModuleMainRedirectManager @"bsl-cubeMMRManager"

/// <summary>
/// 由管理页跳入首页
/// </summary>
#define ModuleMainRedirectMain @"bsl-cubeMMRMain"

/// <summary>
/// 更新模块icon
/// </summary>
#define CubeModuleUpdateIconNumber @"cubeCMUINumber"



