//
//  ICubeModule.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import <Foundation/Foundation.h>

@class CubeModuleWorkTask;

@protocol ICubeModuleWorkTask <NSObject>

-(void) success:(CubeModuleWorkTask*)sync;
-(void) process:(CubeModuleWorkTask*)sync;
-(void) failed:(CubeModuleWorkTask*)sync;

@end

@protocol ICubeModuleWorkDownloadedAndUnZip <NSObject>
-(void) process;
-(void) failed;
-(void) unzip;
-(void) success;

@end
