//
//  CubeModuleWorkTask.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import <Foundation/Foundation.h>
#import "ICubeModule.h"


typedef enum {
    CubeModuleWorkTaskActionInstall=0,
    CubeModuleWorkTaskActionUpdate
}CubeModuleWorkTaskAction;


@class CubeModuleDownloaded;
@class CubeModuleUnZip;
@class CubeModel;


//工作组管理者
@interface CubeModuleWorkTask : NSObject<ICubeModuleWorkDownloadedAndUnZip>{
    
    CubeModuleDownloaded* cubeDownloaded;
    CubeModuleUnZip* cubeUnziped;
    
}

@property(nonatomic,strong) NSString* bundle;
@property (nonatomic,assign) CubeModuleWorkTaskAction action;
@property (nonatomic,strong) CubeModel* cubeModel;
@property(nonatomic,assign) BOOL running;
@property(nonatomic,weak) id<ICubeModuleWorkTask> callback;


-(void)cancel;

-(void)start;

@end
