//
//  LoginManager.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import <Foundation/Foundation.h>

#import "ICubeModule.h"

@class CubeModel;

//模块下载管理者
@interface CubeModuleManager : NSObject<ICubeModuleWorkTask>{
    NSMutableArray* asyncs;
    BOOL asyncIsDownloaded;
    
    NSTimer* delayUpdateMainTimer;
}

//同步模块
-(void)syncModuleList:(NSArray*)remoteServiceList;

//获取已下载模块
-(void)loadLocalModuleList;

//保存已下载模块
-(void)saveLocalModuleList;

//查出正在下载的模块
-(CubeModel*)findDownloadingCubeModule:(NSString*)identifier;

//取消所有工作组
-(void)cancel;

//获取工作组总数
-(NSUInteger)downloadCount;

//下载模块
-(void)installModule:(NSString*)identifier;

//删除模块
-(void)uninstallModule:(NSString*)identifier;

//更新模块
-(void)updateModule:(NSString*)identifier;

@end
