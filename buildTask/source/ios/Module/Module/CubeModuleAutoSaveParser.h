//
//  CubeModuleAutoSaveParser.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import <chameleon-ios-sdk/JSONParser.h>

//自动下载json解析
@interface CubeModuleAutoSaveParser : JSONParser

+(NSString*)parserJSONFromArray:(NSArray*)array;

@end
