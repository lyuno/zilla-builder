//
//  AboutViewController.m
//  cube-ios
//
//  Created by 东 on 13-3-20.
//
//

#import "AboutViewController.h"
#import <MainModuleSDK/MainUtils.h>
#import <chameleon-ios-sdk/Library/UIDevice+IdentifierAddition.h>

@interface AboutViewController ()
@end

@implementation AboutViewController

- (id)init{
    self = [super init];
    if (self) {
        self.title = @"关于我们";

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([MainUtils isPad]){
        CGRect rect=self.view.frame;
        rect.size=CGSizeMake(540.0f, 748.0f);
        self.view.frame=rect;
    }

    
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Module.bundle/images/homebackImg.png"]];

    [self initNavigation];
    
    headerView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"114.png"]];
    CGRect rect=headerView.frame;
    rect.origin.y=58.0f+CGRectGetMaxY(navigationBar.frame);
    rect.origin.x=(self.view.frame.size.width-rect.size.width)*0.5f;
    headerView.frame=rect;
    [self.view addSubview:headerView];
    
    versionLabel=[[UILabel alloc] initWithFrame:CGRectMake(0.0f, 188+CGRectGetMaxY(navigationBar.frame), self.view.frame.size.width, 21.0f)];
    versionLabel.textAlignment=NSTextAlignmentCenter;
    versionLabel.font=[UIFont systemFontOfSize:16.0f];
    versionLabel.backgroundColor=[UIColor clearColor];
    versionLabel.textColor=[UIColor grayColor];
    [self.view addSubview:versionLabel];
    
    appNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(0.0f, 226.0f+CGRectGetMaxY(navigationBar.frame), self.view.frame.size.width, 27.0f)];
    appNameLabel.textAlignment=NSTextAlignmentCenter;
    appNameLabel.font=[UIFont boldSystemFontOfSize:22.0f];
    appNameLabel.backgroundColor=[UIColor clearColor];
    appNameLabel.textColor=[UIColor grayColor];
    [self.view addSubview:appNameLabel];
    
    deviceTipLabel=[[UILabel alloc] initWithFrame:CGRectMake(22.0f, 337.0f+CGRectGetMaxY(navigationBar.frame), 280.0f, 21.0f)];
    deviceTipLabel.font=[UIFont systemFontOfSize:17.0f];
    deviceTipLabel.backgroundColor=[UIColor clearColor];
    deviceTipLabel.textColor=[UIColor grayColor];
    [self.view addSubview:deviceTipLabel];

    driviceIdLabel=[[UILabel alloc] initWithFrame:CGRectMake(22.0f, 366.0f+CGRectGetMaxY(navigationBar.frame), 280.0f, 21.0f)];
    driviceIdLabel.font=[UIFont systemFontOfSize:15.0f];
    driviceIdLabel.backgroundColor=[UIColor clearColor];
    driviceIdLabel.textColor=[UIColor grayColor];
    [self.view addSubview:driviceIdLabel];

    
    NSString *shortVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    versionLabel.text = [NSString stringWithFormat:@"当前版本 v%@", shortVersion];
    deviceTipLabel.text=@"设备标识符";
    NSString *deviceID = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    driviceIdLabel.text = deviceID;
    appNameLabel.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];

}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark method

-(void)backClick{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
