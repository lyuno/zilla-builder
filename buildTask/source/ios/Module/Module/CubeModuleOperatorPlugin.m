//
//  CubeModuleOperator.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "CubeModuleOperatorPlugin.h"
#import <chameleon-ios-sdk/ChamleonSDK.h>
#import "CubeModule.h"
#import "BSLCubeConstant.h"
#import "SVProgressHUD.h"


#define NOTPROMISS      1
#define SERVER_ERROR    2


@interface CubeModuleOperatorPlugin()<ZillaDelegate,UIAlertViewDelegate>

@end

@implementation CubeModuleOperatorPlugin{
    Zilla* zillaSDK;
    NSString* callbackId;
}


-(void)sync:(CDVInvokedUrlCommand*)command{
    
    if([callbackId length]>0){
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        return;
    }
    callbackId=command.callbackId;

    
    [SVProgressHUD showWithStatus:@"正在同步..." maskType:SVProgressHUDMaskTypeBlack];
    
    
    
    

    
    if(zillaSDK==nil){
        zillaSDK=[[Zilla alloc] initWithZillAccessData:[ZillaAccessData activeSession]];
        zillaSDK.delegate=self;
    }
    [zillaSDK cancelAllWorkGroup];
    
    [zillaSDK syncModules];

}

-(void)setting:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    [[NSNotificationCenter defaultCenter] postNotificationName:CubeSettingPage object:nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}

-(void)manager:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    [[NSNotificationCenter defaultCenter] postNotificationName:ModuleMainRedirectManager object:nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}

-(void)index:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    [[NSNotificationCenter defaultCenter] postNotificationName:ModuleMainRedirectMain object:nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}

-(void)showModule:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;

    NSString* identifier =  [command.arguments objectAtIndex:0];
    
    NSString* type =  [command.arguments objectAtIndex:1];

    if ([type isEqualToString:@"main"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleWeb object:identifier];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleRunningLanding object:command.arguments];
    }
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}


-(void)install:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    
    NSString* identifier =  [[command.arguments objectAtIndex:0] lowercaseString];
    
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    [module.cubeModuleManager installModule:identifier];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];
}

-(void)upgrade:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    
    NSString* identifier =  [[command.arguments objectAtIndex:0] lowercaseString];
    
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    [module.cubeModuleManager updateModule:identifier];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}


-(void)uninstall:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    
    NSString* identifier =  [[command.arguments objectAtIndex:0] lowercaseString];
    
    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    [module.cubeModuleManager uninstallModule:identifier];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}

-(void)setTheme:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}

-(void)fragmenthide:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}

-(void)showHomeButton:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId = command.callbackId;
    NSString* action=@"1";
    if([command.arguments count]>0)
        action=[NSString stringWithFormat:@"%@",[command.arguments objectAtIndex:0]];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:action];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}


#pragma mark zilla delegate

//应用过期
-(void)authorExpired{
    
    [zillaSDK checkAppVaildate];

}

//模块获取回调
-(void)syncModulesSuccess:(NSArray*)list{
    
    //        [module.messageManager syncUpdateNumber];
    CubeModule* cubeModule=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    [cubeModule.cubeModuleManager syncModuleList:list];
    NSLog(@"同步成功");
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:CubeSyncSuccess object:nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"同步成功"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

    callbackId=nil;
}

-(void)syncModulesFailed:(int)statusCode errorMessage:(NSString*)errorMessage{
    //        [module.messageManager syncUpdateNumber];
    
    NSLog(@"同步失败");
    [SVProgressHUD dismiss];
    
    if(statusCode==400){
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"应用已删除" message:nil delegate:self cancelButtonTitle:@"退出" otherButtonTitles:nil, nil];
        alertView.tag=NOTPROMISS;
        [alertView show];
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:CubeSyncFailed object:nil];
    }
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"同步失败"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}


//应用验证成功
-(void)authorSuccess{
    //再重新同步
    [zillaSDK syncModules];
}

//应用验证失败
-(void)authorFailed:(int)statusCode errorMessage:(NSString *)errorMessage{
    
    if(statusCode==403 || statusCode==400){
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"应用没有授于权限" message:nil delegate:self cancelButtonTitle:@"退出" otherButtonTitles:nil, nil];
        alertView.tag=NOTPROMISS;
        [alertView show];
    }
    else{
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"服务器异常，请重试" message:nil delegate:self cancelButtonTitle:@"退出" otherButtonTitles:@"重试", nil];
        alertView.tag=SERVER_ERROR;
        [alertView show];
    }
}


#pragma mark alertview delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag== SERVER_ERROR){
        if(buttonIndex==1)
            [zillaSDK checkAppVaildate];
        else
            exit(0);
    }
    else if(alertView.tag==NOTPROMISS){
        exit(0);
    }
}





@end
